//
//  Aliases.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 27/01/22.
//

import Foundation
import UIKit
import Alamofire
import SwiftyRSA

typealias ModelReturn = ((JsonObject?)->Void)

let store = UserDefaults.standard
var isRefreshingToken = false

let keyString = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCEkAxWZf6H93l3YKXtdll1Bn7FjPyibeyuVHm6MgKzN0xFWb8sLAeeWC393a6i/kMrxP3m4BzRuXY3sAkkHXY0H+E05e4peI82TZDN7q8YvxXDCyhTmrSWDrY9QStrCeX6DUSM2hscoYrfQCHWIIiTTBYAJelFqAizYeRZ1yDEqQIDAQAB"
 
// let publicKey = try PublicKey(base64Encoded: keyString)
// let clear = try ClearMessage(string: "Clear Text", using: .utf8)
// let encrypted = clear.encrypted(with: publicKey, padding: .PKCS1)
//
//let data = encrypted.data
//let base64String = encrypted.base64String

var isLogin: Bool {
    get { return store.value(forKey: "isLogin") as? Bool ?? false }
    set { store.set(newValue, forKey: "isLogin") }
}

var isEnglish: Bool {
    get { return store.value(forKey: "isEnglish") as? Bool ?? false }
    set { store.set(newValue, forKey: "isEnglish") }
}

var isGuest: Bool {
    get { return store.value(forKey: "isGuest") as? Bool ?? true }
    set { store.set(newValue, forKey: "isGuest") }
}

var isPublik: String? {
    get { return store.value(forKey: "isPublik") as? String }
    set { store.set(newValue, forKey: "isPublik") }
}

var pendingRequest = [RequestObj]()

struct RequestObj {
    var method: Alamofire.HTTPMethod = .get
    var url: String?
    var param: Dict? = nil
    var retryCount: Int = 0
    var callback: ModelReturn?
}

enum ResponseStatus {
    case success200
    case unauthorized401
    case notFound404
    case methodNotAllowed405
    case internalServerError500
    case badGateway502
    case serviceUnavailable503
    case requestTimeout504
    
    case noInternetConnection
    case cannotParseJSON
    case noContent
    case other
    case custom
}

typealias dictStringOpt = Dictionary<String, String?>
typealias dictTypeOpt = Dictionary<String, AnyObject?>

//sheetVc?.preferredContentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height / 2)

