//
//  AppDelegate.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 21/01/22.
//

import UIKit
import IQKeyboardManagerSwift
import DropDown
//import Firebase
import UserNotifications
//import FirebaseInstanceID
//import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate/**,MessagingDelegate**/ {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        UINavigationBar.appearance().barTintColor = .white
//        UINavigationBar.appearance().tintColor = .black
//        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        //UINavigationBar.appearance().isTranslucent = false
        IQKeyboardManager.shared.enable = true
        DropDown.startListeningToKeyboard()
      
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
          //  Messaging.messaging().remoteMessageDelegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
      //  FirebaseApp.configure()
        
        if !isGuest {
            let vc = UIViewController.instantiate(named: "TabBarVc")
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
            return true
        }
        return true
    }

    // MARK: UISceneSession Lifecycle



}

//extension AppDelegate {
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        print("Firebase registration token #1: \(fcmToken)")
//      //  locker.fcmToken = fcmToken
//    }
//
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let tokenParts = deviceToken.map { data -> String in
//            return String(format: "%02.2hhx", data)
//        }
//        let token = tokenParts.joined()
//        print("Token #2: \(token)")
//    }
//
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        print("Remote message #4: \(remoteMessage.appData)")
//    }
//
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
//        print("user info #5: \(userInfo)")
//
//        if UIApplication.shared.applicationState == .active { print("ACTIVE-------") }
//        else if UIApplication.shared.applicationState == .background { print("BACKGROUND-----------"); }
//        else {
//            print("INACTIVE-------------")
//     //       executeNotif(userInfo: userInfo)
//        }
//    }
//
//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        completionHandler([.alert, .sound])
//    }
//
//    @available(iOS 10.0, *)
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        let userInfo = response.notification.request.content.userInfo
//   //     executeNotif(userInfo: userInfo)
//    }
//}
