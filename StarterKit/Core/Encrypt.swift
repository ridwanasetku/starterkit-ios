//
//  Encrypt.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 27/01/22.
//

import Foundation
import SwiftyRSA
import LocalAuthentication

class RSAManager {
    static var shared: RSAManager {
        return RSAManager()
    }
    
    private init() { }
    
    let keyString = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCgFGVfrY4jQSoZQWWygZ83roKXWD4YeT2x2p41dGkPixe73rT2IW04glagN2vgoZoHuOPqa5and6kAmK2ujmCHu6D1auJhE2tXP+yLkpSiYMQucDKmCsWMnW9XlC5K7OSL77TXXcfvTvyZcjObEz6LIBRzs6+FqpFbUO9SJEfh6wIDAQAB"
     
    lazy var publicKey = try! PublicKey(base64Encoded: keyString)
     
    
    func getEncryptedPassword(from password: String) -> String {
        let clear = try! ClearMessage(string: password, using: .utf8)
        let encrypted = try! clear.encrypted(with: publicKey, padding: .PKCS1)
        return encrypted.base64String
    }
    
    
}
