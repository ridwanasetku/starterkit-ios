//
//  Engine.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 27/01/22.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher

let engine = Engine.sharedInstance

class Engine {
    static let sharedInstance = Engine();
    private init () {}
}

extension Engine {
    
    
    static func requestImage(url: String?, silent: Bool = false, callback: ((UIImage?)->Void)?) {
        guard var url = url else { callback? (nil); return; }
        url = Url.completeForFile(url: url);
        guard let urlcok = URL.init(string: url) else {
              return
          }
        print ("Enter Requesting image url : \(url)");
        let resource = ImageResource(downloadURL: urlcok)
        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
            switch result {
               case .success(let value):
                   print("Image: \(value.image). Got from: \(value.cacheType)")
                callback?(value.image)
               case .failure(let error):
                   print("Error: \(error)")
               }
           }
    }
    
    func login(username: String, password: String,callback: Return<OAuthObj?>?) {
        let param = ["username": username as Any, "password": password as Any];
         net.request(method: .post, url: Url.login, param: param){
             callback?(OAuthObj($0));
         }
     }
    
    func getProfile(callback: Return<ProfileObj?>?){
        net.request(url: Url.me){
            callback?(ProfileObj($0))
        }
    }
    
    func register(username: String,email: String,fullname: String,role: String,callback: Return<OAuthObj?>?){
        let param = ["username": username as Any, "email": email as Any, "fullname": fullname as Any, "role": role as Any];
        net.request(method: .post, url: Url.signup, param: param){
            callback?(OAuthObj($0))
        }
    }
    
    //MARK: - User
    func getListUser(page:Int,size: Int,callback: Return<ProfileModel?>?){
        net.request(url: Url.getUser(page: page, size: size)){
            callback?(ProfileModel($0))
        }
    }
    
    func getUserDetail(id: Int,callback: Return<ProfileObj?>?){
        net.request(url: Url.getUserDetail(id: id)){
            callback?(ProfileObj($0))
        }
    }
    
    func resetPassword(id: Int,callback: Return<ProfileObj?>?){
        net.request(url: Url.resetPassword(id: id)){
            callback?(ProfileObj($0))
        }
    }
    
    func updateUser(username: String,fullname: String,email: String, status: Bool,role: String,id: Int,callback: Return<ProfileObj?>?){
        let param = ["username": username as Any, "fullname": fullname as Any, "email": email as Any, "status": status as Any, "role": role as Any]
        net.request(method: .put, url: Url.updateUser(id: id), param: param){
            callback?(ProfileObj($0))
        }
    }
    
    //MARK: - Verify
    func verifyNik(nik: String,callback: Return<VerifyModel?>?){
        let param = ["nik": nik as Any]
        net.request(method: .post, url: Url.verifyNik, param: param){
            callback?(VerifyModel($0))
        }
    }
    
    func registerNik(nik: String,name: String,birthdate: String, birthplace: String,role: String, callback: Return<VerifyObj?>?){
        let param = ["nik": nik as Any, "name": name as Any,"birthdate": birthdate as Any,"birthplace": birthplace as Any, "role": role as Any]
        net.request(method: .post, url: Url.registerNik, param: param){
            callback?(VerifyObj($0))
        }
    }
    
    func registerOKBank(firstName: String, lastName: String, email: String, password: String, role: String, group: String, callback: Return<VerifyObj?>?){
        let param = ["firstName": firstName as Any, "lastName": lastName as Any, "email": email as Any, "password": password as Any, "role": role as Any, "group": group as Any];
        net.request(method: .post, url: Url.registerAccount, param: param){
            callback?(VerifyObj($0))
        }
    }
    
    func registerGenerateOTPOKBank(email: String, callback: Return<VerifyObj?>?){
        let param = ["email": email as Any];
        net.request(method: .post, url: Url.registerGenerateOTP, param: param){
            callback?(VerifyObj($0))
        }
    }
    
    func registerValidateOTPOKBank(email: String, otp: String, callback: Return<VerifyObj?>?){
        let param = ["email": email as Any, "otp": otp as Any];
        net.request(method: .post, url: Url.registerValidateOTP, param: param){
            callback?(VerifyObj($0))
        }
    }
    
    func loginOKBank(email: String, password: String, callback: Return<VerifyObj?>?){
        let param = ["email": email as Any, "password": password as Any];
        net.request(method: .post, url: Url.loginAccount, param: param){
            callback?(VerifyObj($0))
        }
    }
    
//    func lastTransactionTransfer(page: Int,size: Int,callback: Return<TransactionModelObj?>?){
//        net.request(url: Url.getLastTransactionTransfer(page: page, size: size)){
//            callback?(TransactionModelObj($0));
//        }
//    }
}
