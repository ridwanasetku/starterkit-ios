//
//  Singleton.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 27/01/22.
//

import Foundation

let locker = Singleton.sharedInstance;

class Singleton {
    
    static let sharedInstance = Singleton();
    private init () {}
    
    var store = UserDefaults.standard;
    
}

extension Singleton {
    
    subscript (key: String)->Any? {
        get { return store[key]; }
        set { if newValue != nil { store[key] = newValue! as AnyObject; } }
    }
    
    func saveCollection (key: String?, collection: Any?) {
        if let json = collection, let _key = key {
            store[_key] = nil;
            store.synchronize();
            store[_key] = NSKeyedArchiver.archivedData(withRootObject: json) as AnyObject;
            store.synchronize();
        }
    }
    
    func loadCollection (key: String?) -> Any? {
        if let _key = key, let data = store[_key] as? Data {
         //   return NSKeyedUnarchiver.unarchivedObject(ofClasses: [Any], from: data) as Any;
            return NSKeyedUnarchiver.unarchiveObject(with: data) as Any;
        }
        return nil;
    }
    
}

extension Singleton {
    
    func clear () {
        store.removePersistentDomain(forName: Bundle.main.bundleIdentifier!);
        store.synchronize();
    }
    
    var oauth: OAuthObj? {
        get { return OAuthObj(json: loadCollection(key: #function.snake)); }
        set { saveCollection(key: #function.snake, collection: newValue?.rawData); }
    }
    
    var ieuPublic: String? {
        get { return loadCollection(key: #function) as? String }
        set { saveCollection(key: #function, collection: newValue)}
    }
    
    var signature: String? {
        get { return loadCollection(key: #function) as? String }
        set { saveCollection(key: #function, collection: newValue)}
    }

}

