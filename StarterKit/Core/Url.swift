//
//  Url.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 27/01/22.
//

import Foundation
import UIKit

class Url {
    
    private init() {}
    
    static func makeQuery (dict: dictTypeOpt?)->String {
        return dict?.map({ return $1 == nil ? "" : "\($0)=\($1!)" }).filter({ $0 != "" }).joined(separator: "&").prefixed(by: "?") ?? "";
    }
    
    static func completeForFile (url: String?) -> String {
         guard let url = url, url != "" else { return ""; }
         if url.lowercased().contains(substring: "http") { return url; }
         else { return mainAsli + "/" + url; }
     }
    
    static let mainAsli = "https://app-asliri-springboot.herokuapp.com"
    static let baseURL = "https://asetku-be-starterkit.herokuapp.com"
    
//    static func updateCardNumber(noAccount: String) -> String{
//        return mainMobile + "/api/account/\(noAccount)"
//    }
    
//    static func mutasiRekening(startDate:String?,endDate: String?,accountNo: String) -> String {
//        let dict = ["startDate": startDate as AnyObject,
//                    "endDate": endDate as AnyObject]
//        return mainMobile + "/api/account/\(accountNo)/statement" + makeQuery(dict: dict);
//    }
    
    //Login
    static let login = mainAsli + "/api/v1/auth/signin"
    static let signup = mainAsli + "/api/v1/auth/signup"
    static let logout = mainAsli + "/api/v1/auth/logout"
    
    //Menu
    static let menu = mainAsli + "/api/v1/menu/index"
    static let store = mainAsli + "/api/v1/menu/store"
    
    //Role
    static let role = mainAsli + "/api/v1/role/index"
    static let storeRole = mainAsli + "/api/v1/role/store"
    
    //User
    static let me = mainAsli + "/api/v1/user"
    static func getUser(page:Int?,size: Int?) -> String {
        let dict = ["page": page as AnyObject,
                    "size": size as AnyObject]
        return mainAsli + "/api/v1/user/pagination" + makeQuery(dict: dict);
    }
    
    static func getUserDetail(id: Int) -> String{
        return mainAsli + "/api/v1/user/\(id)"
    }
    
    static func resetPassword(id: Int) -> String{
        return mainAsli + "/api/v1/user/restPassword/\(id)"
    }
    
    static func updateUser(id: Int) -> String{
        return mainAsli + "/api/v1/user/\(id)"
    }
    
    //Verify
    static let verifyNik = mainAsli + "/api/v1/verifytext/check"
    static let registerNik = mainAsli + "/api/v1/verifytext/register"
    
    // Register
    static let registerAccount = baseURL + "/api/account/register"
    static let registerGenerateOTP = baseURL + "/api/account/register/generate-otp"
    static let registerValidateOTP = baseURL + "/api/account/register/validate-otp"
    static let loginAccount = baseURL + "/api/account/signin"
}
