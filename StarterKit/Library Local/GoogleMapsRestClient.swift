//
//  GoogleMapsRestClient.swift
//  KliknClean
//
//  Created by Sailendra Salsabil on 03/07/19.
//  Copyright © 2019 Ridwan. All rights reserved.
//

import Alamofire
import SwiftyJSON

class GoogleMapsRestClient {
    
    static fileprivate func getResponse(url: String, params: [String:String], complete: @escaping (_ response: JSON?) -> Void) {
        
        Alamofire.request(url, method: .get, parameters: params).validate().responseJSON { response in
            
            if let json = response.result.value {
                complete(JSON(json))
            } else {
                complete(nil)
            }
            
            debugPrint(response)
            
        }
        
    }
    
    static func geocodeAddress(params: [String:String], complete: @escaping (_ response: JSON?) -> Void) {
        let url = "https://maps.googleapis.com/maps/api/geocode/json"
        getResponse(url: url, params: params) { (response) in
            complete(response)
        }
    }
    
}
