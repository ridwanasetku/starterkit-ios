//
//  TextField + Outlined.swift
//  Bank Kalteng
//
//  Created by Asad Ardiansyah on 07/07/21.
//

import MaterialComponents
import MaterialComponents.MaterialTextControls_OutlinedTextFieldsTheming

extension MDCOutlinedTextField {
    func setAttribute(placeholder: String?, color: UIColor, isSecured: Bool = false) {
        if let placeholder = placeholder {
            self.label.text = placeholder
        }
        
        self.setTextColor(.black, for: .editing)
        self.setTextColor(.black, for: .normal)

        self.placeholder = placeholder
        self.setOutlineColor(color, for: .editing)
        self.setFloatingLabelColor(color, for: .editing)
    
        self.setOutlineColor(.lightGray, for: .normal)
        self.setFloatingLabelColor(.lightGray, for: .normal)
        self.setNormalLabelColor(.lightGray, for: .normal)
        
        self.isSecureTextEntry = isSecured
    }
    
    func showError(message: String?){
        guard let msg = message else { return }
        print("DEBUG ERROR", msg)
        self.leadingAssistiveLabel.text = msg
        
        self.setLeadingAssistiveLabelColor(.systemRed, for: .normal)
        self.setLeadingAssistiveLabelColor(.systemRed, for: .editing)
        
        self.setOutlineColor(.systemRed, for: .normal)
        self.setOutlineColor(.systemRed, for: .editing)
        
        self.setFloatingLabelColor(.systemRed, for: .normal)
        self.setFloatingLabelColor(.systemRed, for: .editing)
    }
    
    func hideError(initalColor: UIColor){
        self.leadingAssistiveLabel.text = nil
        
        self.setOutlineColor(initalColor, for: .editing)
        self.setOutlineColor(.lightGray, for: .normal)
        
        self.setFloatingLabelColor(initalColor, for: .editing)
        self.setFloatingLabelColor(.lightGray, for: .normal)
        
    }
}
