//
//  UICollectionView+Nearby.swift
//  Bank Kalteng
//
//  Created by Asad Ardiansyah on 14/07/21.
//

import Foundation
import UIKit

extension UICollectionView {
    func scrollToNearestVisibleCollectionViewCell() {
        self.decelerationRate = UIScrollView.DecelerationRate.fast
        let visibleCenterPositionOfScrollView = Float(self.contentOffset.y + (self.bounds.size.width / 2))
        var closestCellIndex = -1
        var closestDistance: Float = .greatestFiniteMagnitude
        for i in 0..<self.visibleCells.count {
            let cell = self.visibleCells[i]
            let cellHeight = cell.bounds.size.height
            let cellCenter = Float(cell.frame.origin.y + cellHeight / 2)

            // Now calculate closest cell
            let distance: Float = fabsf(visibleCenterPositionOfScrollView - cellCenter)
            if distance < closestDistance {
                closestDistance = distance
                closestCellIndex = self.indexPath(for: cell)!.row
            }
        }
        
        if closestCellIndex != -1 {
            self.scrollToItem(at: IndexPath(row: closestCellIndex, section: 0), at: .centeredVertically, animated: true)
            
        }
    }
}


//extension UITableView {
//    func scrollToNearestVisibleCollectionViewCell(completion: ((_ index: Int) -> Void)?) {
//        self.decelerationRate = UIScrollView.DecelerationRate.fast
//        let visibleCenterPositionOfScrollView = Float(self.contentOffset.y + (self.bounds.size.height / 2))
//        var closestCellIndex = -1
//        var closestDistance: Float = .greatestFiniteMagnitude
//        for i in 0..<self.visibleCells.count {
//            let cell = self.visibleCells[i] as! GiroTableViewCell
//            let cellHeight = cell.bounds.size.height
//            let cellCenter = Float(cell.frame.origin.y + cellHeight / 2)
//
//            // Now calculate closest cell
//            let distance: Float = fabsf(visibleCenterPositionOfScrollView - cellCenter)
//            if distance < closestDistance {
//                closestDistance = distance
//                closestCellIndex = self.indexPath(for: cell)!.row
//            }
//        }
//        
//        if closestCellIndex != -1 {
//            self.scrollToRow(at: IndexPath(row: closestCellIndex, section: 0), at: .middle, animated: true)
//            completion?(closestCellIndex)
//        }
//    }
//}
