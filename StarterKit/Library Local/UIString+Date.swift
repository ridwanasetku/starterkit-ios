//
//  UIString+Date.swift
//  UBM
//
//  Created by Ridwan Surya Putra on 8/8/18.
//  Copyright © 2018 Ridwan Surya Putra. All rights reserved.
//

import Foundation
import UIKit
extension String {
    
    static func getIndonesiaDay(_ hari: String?)->String? {
        guard let hari = hari else { return nil; }
        switch hari {
        case "monday":
            return "Jan"
        case "tuesday":
            return "Feb"
        case "wednesday":
            return "Mar"
        case "thursday":
            return "Apr"
        case "friday":
            return "May"
        case "saturday":
            return "Jun"
        case "sunday":
            return "Jul"
        default:
            return ""
        }
    }
    func convertDateWithHour() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        
        if let date = dateFormatterGet.date(from: self){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            let day = dateFormatter.string(from: date).capitalized
            
            let hasil = "\(day), \(dateFormatterPrint.string(from: date))"
            print(dateFormatterPrint.string(from: date))
            return hasil
        }
        else {
            print("There was an error decoding the string")
            return "-"
        }
    }
    func convertDateWithHourToDate() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        
        if let date = dateFormatterGet.date(from: self){
            let hasil = "\(dateFormatterPrint.string(from: date))"
            print(dateFormatterPrint.string(from: date))
            return hasil
        }
        else {
            print("There was an error decoding the string")
            return "-"
        }
    }
    
    func convertDateWithHourToDateCreate() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMMM yyyy HH:mm:ss"
        
        
        if let date = dateFormatterGet.date(from: self){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.locale = NSLocale.current
            let day = dateFormatter.string(from: date).capitalized
            
            let hasil = "\(day), \(dateFormatterPrint.string(from: date))"
            print(dateFormatterPrint.string(from: date))
            return hasil
        }
        else {
            print("There was an error decoding the string")
            return "-"
        }
    }
    
    func convertDateWithSlash() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        
        if let date = dateFormatterGet.date(from: self){
            let hasil = "\(dateFormatterPrint.string(from: date))"
            print(dateFormatterPrint.string(from: date))
            return hasil
        }
        else {
            print("There was an error decoding the string")
            return "-"
        }
    }
    func convertDateWithHourToDateOnly() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy"
        
        if let date = dateFormatterGet.date(from: self){
            let hasil = "\(dateFormatterPrint.string(from: date))"
            print(dateFormatterPrint.string(from: date))
            return hasil
        }
        else {
            print("There was an error decoding the string")
            return "-"
        }
    }
    func convertDateWithHourToMonth() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy HH:mm:ss"
        
        if let date = dateFormatterGet.date(from: self){
            let hasil = "\(dateFormatterPrint.string(from: date))"
            print(dateFormatterPrint.string(from: date))
            return hasil
        }
        else {
            print("There was an error decoding the string")
            return "-"
        }
    }
    
    func convertDateWithHourToDateHourMinute() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy HH:mm"
        
        if let date = dateFormatterGet.date(from: self){
            let hasil = "\(dateFormatterPrint.string(from: date))"
            print(dateFormatterPrint.string(from: date))
            return hasil
        }
        else {
            print("There was an error decoding the string")
            return "-"
        }
    }
    func convertDateWithHourToYear() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy"
        
        if let date = dateFormatterGet.date(from: self){
            let hasil = "\(dateFormatterPrint.string(from: date))"
            print(dateFormatterPrint.string(from: date))
            return hasil
        }
        else {
            print("There was an error decoding the string")
            return "-"
        }
    }
    
    func convertDate() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        
        if let date = dateFormatterGet.date(from: self){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            let day = dateFormatter.string(from: date).capitalized
            
            let hasil = "\(day), \(dateFormatterPrint.string(from: date))"
            print(dateFormatterPrint.string(from: date))
            return hasil
        }
        else {
            print("There was an error decoding the string")
            return "-"
        }
    }
}
