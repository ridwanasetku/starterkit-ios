//
//  UIView+Form.swift
//  Queue Manager
//
//  Created by Ridwan Surya Putra on 02/05/18.
//  Copyright © 2018 idesolusiasia. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func setBorderForForm() {
        
        self.layer.borderColor = UIColor.init(rgbHex: 0x102B72).alpha(0.3).cgColor;
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 5;
        self.clipsToBounds = true;
        
    }
    
    func setborderwhite(){
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 6;
        self.clipsToBounds = true;
    }
    
    func setborder(){
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 5;
        self.clipsToBounds = true;
    }
    
    func setborderGray(){
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 8;
        self.clipsToBounds = true;
    }
    
    func setborderGold(){
        self.layer.borderColor = UIColor.init(rgbHex: 0xF1901F).alpha(1.0).cgColor
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 8;
        self.clipsToBounds = true;
    }
    
//    func setborderGolden(){
//        self.layer.borderColor = UIColor.redKB.cgColor
//        self.layer.borderWidth = 1;
//        self.layer.cornerRadius = 8;
//        self.clipsToBounds = true;
//    }
    
    func setBorderHistory(){
        self.layer.borderColor = UIColor.init(rgbHex: 0xC7A87E).alpha(1.0).cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 6
        self.clipsToBounds = true
    }
    
}

extension UIColor {
    
    func alpha(_ a: CGFloat) -> UIColor { return self.withAlphaComponent(a); }
    
}
