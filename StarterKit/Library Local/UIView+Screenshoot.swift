//
//  UIView+Screenshoot.swift
//  Bank Kalteng
//
//  Created by Ridwan Surya Putra on 11/11/21.
//

import Foundation
import UIKit

extension UIView {
    func toImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)

        drawHierarchy(in: self.bounds, afterScreenUpdates: true)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
