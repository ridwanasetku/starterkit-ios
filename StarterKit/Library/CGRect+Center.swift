//
//  CGRect+Center.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
import UIKit

extension CGRect {
    
    var center: CGPoint {
        return CGPoint.init(x: (origin.x + size.width) / 2, y: (origin.y + size.height) / 2);
    }
    
}
