//
//  CGSize+ProportionalScalling.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
import UIKit

extension CGSize {
    
    func scaledHeight (forWidth: CGFloat) -> CGFloat { return height / width * forWidth; }
    func scaledWidth (forHeight: CGFloat) -> CGFloat { return width / height * forHeight; }
    
}
