//
//  Collection+AppendUnique.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    @discardableResult
    mutating func appendIfNew(_ element: Element) -> Bool {
        if !contains(element) { append(element); return true; }
        return false;
    }
    
}
