//
//  Collection+Comparison.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
import UIKit

public func == (lhs: [String]?, rhs: [String]? ) -> Bool {
    if lhs == nil && rhs == nil { return true; }
    guard let lhs = lhs, let rhs = rhs else { return false; }
    if lhs.count != rhs.count { return false; }
    for (idx, el) in lhs.enumerated() { if rhs[idx] != el { return false; } }
    return true;
}

public func == (lhs: [Int]?, rhs: [Int]? ) -> Bool {
    if lhs == nil && rhs == nil { return true; }
    guard let lhs = lhs, let rhs = rhs else { return false; }
    if lhs.count != rhs.count { return false; }
    for (idx, el) in lhs.enumerated() { if rhs[idx] != el { return false; } }
    return true;
}

public func == (lhs: [Double]?, rhs: [Double]? ) -> Bool {
    if lhs == nil && rhs == nil { return true; }
    guard let lhs = lhs, let rhs = rhs else { return false; }
    if lhs.count != rhs.count { return false; }
    for (idx, el) in lhs.enumerated() { if rhs[idx] != el { return false; } }
    return true;
}

public func == (lhs: [CGFloat]?, rhs: [CGFloat]? ) -> Bool {
    if lhs == nil && rhs == nil { return true; }
    guard let lhs = lhs, let rhs = rhs else { return false; }
    if lhs.count != rhs.count { return false; }
    for (idx, el) in lhs.enumerated() { if rhs[idx] != el { return false; } }
    return true;
}

public func == (lhs: [Float]?, rhs: [Float]? ) -> Bool {
    if lhs == nil && rhs == nil { return true; }
    guard let lhs = lhs, let rhs = rhs else { return false; }
    if lhs.count != rhs.count { return false; }
    for (idx, el) in lhs.enumerated() { if rhs[idx] != el { return false; } }
    return true;
}
