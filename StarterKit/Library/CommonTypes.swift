//
//  CommonTypes.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 11/04/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

typealias Dict = [String: Any];
typealias DictOpt = [String: Any?];
typealias DictObj = [String: AnyObject];
typealias Arr = [Dict];
typealias DictString = [String: String];
typealias DictStringOpt = [String: String?];
typealias ArrString = Array<String>
typealias ArrInt = Array<Int>
typealias ArrBool = Array<Bool>
typealias Return<T> = ((T)->Void)
typealias Change<T> = ((T)->T)
typealias Callback = (()->Void);
typealias Rereturn<T> = ((T)->((T)->Void)?)
