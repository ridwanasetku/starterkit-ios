//
//  Currency.swift
//  Bank Kalteng
//
//  Created by Ridwan Surya Putra on 29/07/21.
//

import Foundation
import UIKit

private var previousTextFieldContent: String?
private var previousSelection: UITextRange?
private var originNumber = ""

extension UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        previousTextFieldContent = textField.text
        previousSelection = textField.selectedTextRange
        return true
    }

    func reformatNumber(textField: UITextField) {
        var targetCursorPosition = 0
        if let startPosition = textField.selectedTextRange?.start {
            targetCursorPosition = textField.offset(from: textField.beginningOfDocument, to: startPosition)
        }

        var numberWithoutDivider = ""
        if let text = textField.text {
            numberWithoutDivider = self.removeNonDigits(string: text, andPreserveCursorPosition: &targetCursorPosition)
        }
        
        let numberWithDivider = self.insertDivider(numberWithoutDivider, preserveCursorPosition: &targetCursorPosition)
        textField.text = numberWithDivider

        if let targetPosition = textField.position(from: textField.beginningOfDocument, offset: targetCursorPosition) {
            textField.selectedTextRange = textField.textRange(from: targetPosition, to: targetPosition)
        }
        originNumber = numberWithoutDivider
    }

    func removeNonDigits(string: String, andPreserveCursorPosition cursorPosition: inout Int) -> String {
        var digitsOnlyString = ""
        let originalCursorPosition = cursorPosition

        for i in Swift.stride(from: 0, to: string.count, by: 1) {
            let characterToAdd = string[string.index(string.startIndex, offsetBy: i)]
            if characterToAdd >= "0" && characterToAdd <= "9" {
                digitsOnlyString.append(characterToAdd)
            }
            else if i < originalCursorPosition {
                cursorPosition -= 1
            }
        }

        return digitsOnlyString
    }

    func insertDivider(_ string: String, divider: String = ".", preserveCursorPosition cursorPosition: inout Int) -> String {
        var stringWithAddedDivider = ""
        let cursorPositionInSpacelessString = cursorPosition

        for i in 0..<string.count {
            if i > 0, i % 3 == 0{
                stringWithAddedDivider.append(divider)
                if i < cursorPositionInSpacelessString {
                    cursorPosition += 1
                }
            }

            let characterToAdd = string[string.index(string.startIndex, offsetBy:i)]
            stringWithAddedDivider.append(characterToAdd)
        }

        return stringWithAddedDivider
    }
}
//END MARK
