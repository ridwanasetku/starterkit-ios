//
//  Date+Comparison.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    
    var isExpired: Bool { return self < Date(); }
    
}
