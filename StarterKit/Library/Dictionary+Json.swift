//
//  Dictionary+Json.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 11/04/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension Dictionary {
    
    var jsonData : Data? {
        get {
            do { return try JSONSerialization.data(withJSONObject: self, options: []); }
            catch { print("\n\n\nJSON error: \(error.localizedDescription)"); return nil; }
        }
    }
}

extension Data {
    
    var json : [String: Any]? {
        do { return try JSONSerialization.jsonObject(with: self, options: []) as? [String: Any]; }
        catch { print("\n\n\nJSON error: \(error.localizedDescription)"); return nil; }
    }
    
    var string : String? {
        return String(data: self, encoding: String.Encoding.utf8)?.replacingOccurrences(of: "\\", with: "").replacingOccurrences(of: "\"", with: "");}
}
