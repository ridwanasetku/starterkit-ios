//
//  IndexPath+MockupShortcut.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension IndexPath {
    
    var code : String { return "\(self.section)-\(self.row)"; }
    func isEqual (codeCompare: String)->Bool { return self.code == codeCompare; }
    
}
