//
//  Int+DecimalFormatter.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension Int{
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
    func toCurrency() -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: self as NSNumber) {
            let hapusKoma = formattedTipAmount.replacingOccurrences(of: ",00", with: "")
            return hapusKoma.replacingOccurrences(of: "Rp", with: "Rp ")
        }
        return "Rp 0"
    }
    func toCurrencyIDR() -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: self as NSNumber) {
            let hapusKoma = formattedTipAmount.replacingOccurrences(of: ",00", with: "")
            return hapusKoma.replacingOccurrences(of: "Rp", with: "Rp ")
        }
        return "0"
    }
}
