//
//  LazyMapConnection.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

public extension LazyMapCollection  {
    
    func toArray() -> [Element]{
        return Array(self)
    }
}
