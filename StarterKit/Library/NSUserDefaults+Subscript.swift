//
//  NSUserDefaults+Subscript.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    subscript (key: String)->AnyObject? {
        get {
            return self.value(forKey: key) as AnyObject?;
        }
        set {
            self.setValue(newValue, forKey: key);
        }
    }
    
}

