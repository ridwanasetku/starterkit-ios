//
//  NotificationCenter+AddObserver.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
import UIKit

extension NotificationCenter {
    
    open func addObserver(_ observer: Any?, selector: Selector?, name: String?, object: Any? = nil) {
        guard let observer = observer, let selector = selector, let name = name else { return; }
        addObserver(observer, selector: selector, name: NSNotification.Name(name), object: object);
    }

    static public func addObserver(_ observer: Any?, selector: Selector?, name: String?, object: Any? = nil) {
        guard let observer = observer, let selector = selector, let name = name else { return; }
        self.default.addObserver(observer, selector: selector, name: NSNotification.Name(name), object: object);
    }
    
}

func addObserver(_ observer: Any?, selector: Selector?, name: String?, object: Any? = nil) {
    guard let observer = observer, let selector = selector, let name = name else { return; }
    NotificationCenter.default.addObserver(observer, selector: selector, name: NSNotification.Name(name), object: object);
}

extension NSObject {
    var className: String { return NSStringFromClass(type(of: self)) }
}
