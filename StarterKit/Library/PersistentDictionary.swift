//
//  PersistentDictionary.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

class PersistentDictionary <T> {
    
    var dict : Dictionary <String, T> = [:];
    
    init? (key: String?) {
        guard let key = key else { return nil; }
        let dict = UserDefaults.standard.object(forKey: key);
        self.dict = (dict as? Dictionary <String, T>) ?? [:];
    }
    
    subscript (key: String)->T? {
        get { return dict[key]; }
        set { dict[key] = newValue;
            UserDefaults.standard.setValue(dict as AnyObject, forKey: key);
            UserDefaults.standard.synchronize();
        }
    }

    
}
