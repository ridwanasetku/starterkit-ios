//
//  String+Case.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 11/04/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension String {
    
    var snake: String {
        var result = "";
        for i in 0..<self.count {
            let ch = String(self[self.index(self.startIndex, offsetBy: i)]);
            if ch.lowercased() != ch { result += "_" }
            result += ch.lowercased();
        }
        return result;
    }
    
    var camel: String {
        var result = ""
        let items = self.components(separatedBy: "_");
        items.enumerated().forEach {
            result += ((0 == $0.offset) ? $0.element : $0.element.capitalized)
        }
        return result
    }
    
    func toCurrency() -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID") // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: Double(self)! as NSNumber) {
            let hapusKoma = formattedTipAmount.replacingOccurrences(of: ",00", with: "")
            return hapusKoma.replacingOccurrences(of: "Rp", with: "Rp ")
        }
        return "Rp 0"
    }
    
    func currencyFormatting() -> String {
        if let value = Double(self) {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.maximumFractionDigits = 2
            formatter.minimumFractionDigits = 2
            if let str = formatter.string(for: value) {
                return str
            }
        }
        return ""
    }
    
    func getDate() -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from:self)
    }
    
}
