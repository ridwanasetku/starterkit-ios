//
//  String+Contains.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension String {
    
    func contains (substring: String, caseSensitive: Bool = false)->Bool {
        if caseSensitive { return self.range(of:substring) != nil; }
        else { return self.lowercased().range(of:substring.lowercased()) != nil; }
    }
    
}
