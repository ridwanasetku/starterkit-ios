//
//  String+CutOff.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 11/04/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension String {
    
    func cutOff (max: Int, hardCut: Bool = false) -> String {
        if self.count < max { return self; }
        var pos = -1;
        var ellipsis = "";
        for (idx, c) in self.enumerated() {
            if String(c) == " " { pos = idx; }
            if idx > max { ellipsis = "..."; break; }
        }
        if pos == -1 { pos = max; }
        if hardCut { pos = min (max, self.count); }
        
        return String(self.prefix(pos)) + ellipsis;
    }
    
}
