//
//  String+Delete.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
extension String {
    
    mutating func delete (text: String) {
        self = self.replace(text, with: "");
    }
    
}
