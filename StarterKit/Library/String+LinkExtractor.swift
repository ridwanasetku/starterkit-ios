//
//  String+LinkExtractor.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
extension String {
    
    func extractURLs() -> [URL] {
        var urls : [URL] = []
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            detector.enumerateMatches(in: self,
                                      options: [],
                                      range: NSMakeRange(0, count),
                                      using: { (result, _, _) in
                                        if let match = result, let url = match.url {
                                            urls.append(url)
                                        }
            })
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return urls
    }
    
}
