//
//  String+Padding.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension String {
    
    func padding (minCount: Int, withPad: String = " ") -> String {
        var res = self;
        while res.count < minCount { res += " "; }
        return res;
    }
    
}
