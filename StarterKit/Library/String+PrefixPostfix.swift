//
//  String+PrefixPostfix.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 11/04/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension String {
    
    func prefixed (by: String?) -> String {
        return (by ?? "") + self;
    }

    func postfixed (by: String?) -> String {
        return self + (by ?? "");
    }

}
