//
//  String+Replace.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension String {
    
    func replace (_ text: String, with: String) -> String {
        return replacingOccurrences(of: text, with: with);
    }
    
}
