//
//  String+Trim.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation

extension String {
    
    /** Erase all leading and trailing whitespaces */
    func trim() -> String { return self.trimmingCharacters(in: .whitespacesAndNewlines); }
    
}
