//
//  String+Validation.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 24/06/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
extension String {
    
    var isValidEmail : Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}";
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx);
        return emailTest.evaluate(with: self);
    }
    
}
