//
//  UICollectionView+Cell.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 18/04/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    
    func cell (_ name: String? = "cell", indexPath: IndexPath) -> UICollectionViewCell? {
        guard let name = name else { return nil; }
        return dequeueReusableCell(withReuseIdentifier: name, for: indexPath);
    }
    
    func cell (section: Int?, row: Int?) -> UICollectionViewCell? {
        guard let section = section, let row = row else { return nil; }
        return self.cellForItem(at: IndexPath(row: row, section: section));
    }
    
    func cell (indexPath: IndexPath?) -> UICollectionViewCell? {
        guard let indexPath = indexPath else { return nil; }
        return self.cellForItem(at: indexPath);
    }
}

/** Indexpath */
extension UICollectionView {
    
    func exists(indexPath:IndexPath) -> Bool {
        if indexPath.section >= self.numberOfSections { return false }
        if indexPath.row >= self.numberOfItems(inSection: indexPath.section) { return false }
        return true
    }

    func indexPath (for view: UIView?) -> IndexPath? {
        guard let cell = view?.parentCollectionViewCell else { return nil; }
        return indexPath(for: cell)
    }
    
    func sectionNumber (for view: UIView?) -> Int? {
        guard let view = view else { return nil; }
        for i in 0..<numberOfSections {
            if let header = supplementaryView(forElementKind: UICollectionView.elementKindSectionHeader, at: IndexPath.init(row: 0, section: i)) { // might be broken
                if header == view { return i; }
                if view.isDescendant(of: header) { return i; }
            }
        }
        return nil;
    }
}
