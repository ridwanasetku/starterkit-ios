//
//  UIColor+Constant.swift
//  Medscope
//
//  Created by Ridwan Surya Putra on 02/07/20.
//  Copyright © 2020 Ridwan Surya Putra. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    /** The color of a tooltip */
    static var infoTop : UIColor { return UIColor.init(red: 72.0/255.0, green: 198.0/255.0, blue: 239.0/255.0, alpha: 1); }
    static var infoBottom : UIColor { return UIColor.init(red: 111.0/255.0, green: 134.0/255.0, blue: 214.0/255.0, alpha: 1); }
    static var newsTop : UIColor { return UIColor.init(red: 0.0/255.0, green: 92.0/255.0, blue: 134.0/255.0, alpha: 1); }
    static var newsBottom : UIColor { return UIColor.init(red: 220.0/255.0, green: 121.0/255.0, blue: 171.0/255.0, alpha: 1); }
    static var simulationTop : UIColor { return UIColor.init(red: 247.0/255.0, green: 140.0/255.0, blue: 160.0/255.0, alpha: 1); }
    static var simulationBottom : UIColor { return UIColor.init(red: 254.0/255.0, green: 154.0/255.0, blue: 139.0/255.0, alpha: 1); }
    static var contactTop : UIColor { return UIColor.init(red: 146.0/255.0, green: 254.0/255.0, blue: 157.0/255.0, alpha: 1); }
    static var contactBottom : UIColor { return UIColor.init(red: 0.0/255.0, green: 201.0/255.0, blue: 255.0/255.0, alpha: 1); }
    static var atmTop : UIColor { return UIColor.init(red: 195.0/255.0, green: 74.0/255.0, blue: 54.0/255.0, alpha: 1); }
    static var atmBottom : UIColor { return UIColor.init(red: 47.0/255.0, green: 72.0/255.0, blue: 88.0/255.0, alpha: 1); }
    static var bantuanTop : UIColor { return UIColor.init(red: 250.0/255.0, green: 139.0/255.0, blue: 255.0/255.0, alpha: 1); }
    static var bantuanBottom : UIColor { return UIColor.init(red: 43.0/255.0, green: 255.0/255.0, blue: 136.0/255.0, alpha: 1); }
 
    
    
    
    static var redTabBar : UIColor { return UIColor.init(red: 214.0/255.0, green: 86.0/255.0, blue: 121.0/255.0, alpha: 1); }
    static var blueTabBar : UIColor { return UIColor.init(red: 71.0/255.0, green: 174.0/255.0, blue: 244.0/255.0, alpha: 1); }
    static var blueButton : UIColor { return UIColor.init(rgbHex: 0x0099FF); }
    
    static func white(opacity: CGFloat) -> UIColor { return UIColor.init(red: 1, green: 1, blue: 1, alpha: opacity); }
    static func black(opacity: CGFloat) -> UIColor { return UIColor.init(red: 28.0/255.0, green: 26.0/255.0, blue: 27.0/255.0, alpha: opacity); }
    static var gold: UIColor { return UIColor.init(rgbHex: 0xF1F1F1) }
    static var info: UIColor { return UIColor.init(rgbHex: 0x48c6ef)}
    static var blueKalteng : UIColor { return UIColor.init(rgbHex: 0x1A237E)}
    static var greenKalteng : UIColor { return UIColor.init(rgbHex: 0x43A047)}
    static var greenLogo: UIColor { return UIColor.init(rgbHex: 0x008D49)}
    static var redKalteng : UIColor { return UIColor.init(rgbHex: 0xE53935)}
    static var yellowKalteng : UIColor { return UIColor.init(rgbHex: 0xFDD835)}
    
    static var orangeDefault : UIColor { return UIColor.init(rgbHex: 0xF1901F)}

}


