//
//  UITableView+Refreshcontrol.swift
//  Medscope
//
//  Created by Ridwan Surya Putra on 14/07/20.
//  Copyright © 2020 Ridwan Surya Putra. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func addRefreshControl (_ target: Any, action: Selector) -> UIRefreshControl {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(target, action: action, for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.gray;
        self.refreshControl = refreshControl;
        return refreshControl
    }
    
}
