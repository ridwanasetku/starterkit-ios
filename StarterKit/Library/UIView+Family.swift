//
//  UIView+Family.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 19/04/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
import UIKit

extension UIView : UIGestureRecognizerDelegate {
    
    func addTapGestureRecognizer(_ target: Any?, action: Selector?) {
        self.isUserInteractionEnabled = true;
        let tap = UITapGestureRecognizer(target: target, action: action);
        tap.delegate = self
        self.addGestureRecognizer(tap)
    }
    
}
