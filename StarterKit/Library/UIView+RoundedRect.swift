//
//  UIView+RoundedRect.swift
//  LearnFlux
//
//  Created by Martin Tjandra on 8/4/17.
//  Copyright © 2017 Martin Darma Kusuma Tjandra. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
//    func makeRoundedRect(withCornerRadius cornerRadius: CGFloat) {
//        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0);
//        let path = UIBezierPath.init(roundedRect: self.bounds, cornerRadius: cornerRadius);
//        path.addClip();
//        self.image?.draw(in: self.bounds);
//        let finalImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        self.image = finalImage;
//        self.clipsToBounds = true;
////        [[UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:cornerRadius] addClip];
////        [self.image drawInRect:self.bounds];
////        UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
////        UIGraphicsEndImageContext();
////        
////        self.image = finalImage;
//
//    }

    func makeRoundedRect(withCornerRadius cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius;
        self.clipsToBounds = true;
    }
    
    open func makeRounded() {
        let corner = min (self.frame.size.width, self.frame.size.height) / 2;
        self.makeRoundedRect(withCornerRadius: corner);
    }
    
    func makeNonRounded() {
        self.layer.cornerRadius = 0;
    }
    
}

extension UIView {
    /** r = radius, a = alpha, c = color */
    func setShadow (x: CGFloat = 0.5, y: CGFloat = 0.5, r: CGFloat = 4.0, a: Float = 0.5, c: UIColor = UIColor.lightGray) {
        
        layer.shadowOffset = CGSize.init(width: x, height: y);
      //  layer.shadowOffset = .zero
        layer.shadowColor = c.cgColor;
        layer.shadowRadius = r;
        layer.shadowOpacity = a;
      //  layer.masksToBounds = false
    }
    
    
    func noShadow () {
        layer.shadowOpacity = 0;
    }
    
}
