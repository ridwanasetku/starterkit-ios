//
//  UIViewController+Push.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 20/04/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
import UIKit
import Toaster
import FittedSheets

extension UIViewController {
    
    func push (vcNamed: String?, callback: ((UIViewController?)->Void)? = nil) {
        guard let vcNamed = vcNamed else { callback?(nil); return; }
        let vc = UIViewController.instantiate(named: vcNamed)
        self.view.endEditing(true);
        self.view.window?.endEditing(true);
        self.navigationController?.pushViewController(vc!, animated: true)
        callback?(vc);
    }
    
    func push (vc: UIViewController?, callback: ((UIViewController?)->Void)? = nil) {
        guard let vc = vc else { toast ("Vc to be pushed is nil."); return; }
        guard let navcon = self.navigationController else { toast ("Can't get navcon. Missing container?"); return; }
        self.view.endEditing(true);
        self.view.window?.endEditing(true);
        navcon.pushViewController(vc, animated: true);
        callback? (vc);
    }
    
    func pop (animated: Bool = true) {
        self.navigationController?.popViewController(animated: animated);
    }

    func popToRoot (animated: Bool = true) {
        self.navigationController?.popToRootViewController(animated: animated);
    }

    @discardableResult
    func show (from vc: UIViewController?) -> UIViewController? {
        guard let navcon = vc?.navigationController ?? vc as? UINavigationController else { showToast(text: "Can't get navcon. Missing container?"); return self; }
        navcon.pushViewController(self, animated: true);
        return self;
    }
    
    @discardableResult
    func showPop (from vc: UIViewController?) -> UIViewController? {
        guard let navcon = vc?.navigationController ?? vc as? UINavigationController else { showToast(text: "Can't get navcon. Missing container?"); return self; }
        navcon.pushViewController(self, animated: false);
        return self;
    }
    
    func presentInSheet(from vc: UIViewController) {
        let sheetVc = SheetViewController(controller: self)
        sheetVc.extendBackgroundBehindHandle = true
        sheetVc.topCornersRadius = 10
        sheetVc.setSizes([.halfScreen, .fullScreen])
        vc.present(sheetVc, animated: true, completion: nil)
    }
}
