//
//  UIViewController+StoryboaRD.swift
//  Little Hannah
//
//  Created by Ridwan Surya Putra on 20/04/20.
//  Copyright © 2020 Ridwan. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    static func instantiate (named: String?, fromStoryboard: String? = nil) -> UIViewController? {
        guard let named = named else { return nil; }
        let named2 = named.capitalizingFirstLetter();
        if let sbName = fromStoryboard {
            let sb = UIStoryboard(name: sbName, bundle: nil);
            return sb.instantiateViewControllerSafe(withIdentifier: named) ??
                sb.instantiateViewControllerSafe(withIdentifier: named2)
        }
        else {
            let sbs = UIStoryboard.storyboards;
            for sb in sbs {
                let vc = sb.instantiateViewControllerSafe(withIdentifier: named) ??
                    sb.instantiateViewControllerSafe(withIdentifier: named2);
                if vc != nil { return vc; }
            }
        }
        return nil;
    }
}
