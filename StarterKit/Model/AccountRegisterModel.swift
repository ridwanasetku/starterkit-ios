//
//  AccountRegisterModel.swift
//  StarterKit
//
//  Created by Stella Patricia on 15/03/22.
//

import Foundation

class AccountRegisterModel {
    var firstName: String?
    var lastName: String?
    var email: String?
    var password: String?
    var role: String?
    var group: String?
}
