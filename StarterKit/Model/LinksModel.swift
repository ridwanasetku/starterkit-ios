//
//  LinksModel.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 27/01/22.
//

import Foundation
import UIKit
import QuickLookThumbnailing

class LinkString {
    
    var url : String;
    private var cache : AnyObject?;
    init? (url: String?) {
        guard let url = url else { return nil; }
        // self.url = NewUrl.completeForFile(url: url);
        self.url = url
    }
    
//    func getImage (silent: Bool = false, callback: Return<UIImage?>?) {
//      //  if let cache = cache as? UIImage { callback?(cache); return; }
//        Engine.requestImage(url: url, silent: silent) {
//      //      self.cache = $0;
//            //            cached.setImageSize(url: self.url, size: $0?.size);
//            callback?($0);
//        }
//    }
    
//    func getImageLogo (silent: Bool = false, callback: Return<UIImage?>?) {
//        Engine.requestImageLogo(url: url, silent: silent) {
//            callback?($0);
//        }
//    }
//    
//    func getImageProfile (silent: Bool = false, callback: Return<UIImage?>?) {
//        Engine.requestImageProfile(url: url, silent: silent) {
//            callback?($0);
//        }
//    }
    
    
    func downloadFile(fileName: String, callback: @escaping (_ success: Bool, _ fileLocation: URL?)->Void) {
        let fileUrl = URL(string: self.url)
        var request = URLRequest(url: fileUrl!)
        request.setValue("in", forHTTPHeaderField: "Accept-Language")
        request.setValue("Bearer \(locker.oauth?.accessToken ?? "")", forHTTPHeaderField: "Authorization")
        
        //create document folder
        let documentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        //create destination file url
        let destinationUrl = documentsDirectoryURL.appendingPathComponent(fileName)
        
        //check if it exist before download
        if FileManager.default.fileExists(atPath: destinationUrl.path) {
            debugPrint("The file already exist at path")
             Util.mainThread { callback(true, destinationUrl) }
            
        } else {
            URLSession.shared.downloadTask(with: request) { (location, response, error) in
                guard let tempLocation = location, error == nil else {
                    return
                }
                do {
                    //after download, move to destination url
                    try FileManager.default.moveItem(at: tempLocation, to: destinationUrl)
                    print("File moved to documents folder")
                    Util.mainThread {
                        callback(true, destinationUrl)
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                    Util.mainThread {
                        callback(false, nil)
                    }
                }
            }.resume()
        }
    }
    
    private func getThumbnail(size: CGSize, url: URL, callback: Return<UIImage?>?) {
        let request = QLThumbnailGenerator.Request(fileAt: url, size: size, scale: (UIScreen.main.scale), representationTypes: .all)
        let generator = QLThumbnailGenerator.shared
        generator.generateRepresentations(for: request) { (thumbnail, type, error) in
           DispatchQueue.main.async {
               if thumbnail == nil || error != nil {
                   callback?(#imageLiteral(resourceName: "file"))
               } else {
                   DispatchQueue.main.async { // << required !!
                       callback?(thumbnail?.uiImage ?? #imageLiteral(resourceName: "file"))
                   }
               }
           }
       }
    }
    
    func generateThumbnail(size: CGSize, fileName: String, callback: Return<UIImage?>?) {
        self.downloadFile(fileName: fileName, callback: { (success, locationUrl) in
            if success {
                self.getThumbnail(size: size, url: locationUrl!, callback: callback)
            } else {
                callback?(nil)
            }
        })
    }
}

class LinksString {
    
    var urls : [String];
    private var cache : AnyObject?;
    init? (urls: [String]?) {
        guard let urls = urls else { return nil; }
        self.urls = urls;
    }
    
    subscript (key: Int?)->LinkString? {
        get {
            guard let key = key else { return nil; }
            return LinkString(url: urls[safe: key]);
        }
    }
    
}

class LinkObj {
    
    var id : String;
    init?(id: String?) {
        guard let id = id else { return nil; }
        self.id = id;
    }
    init?(id: AnyObject?) {
        guard let id = id as? String else { return nil; }
        self.id = id;
    }
    
}

//class LinkUser : LinkObj {
//    func get (callback: Return<UserDetailObj?>?) { Engine.getUser(id: id, callback: callback); } }
//class LinkProject : LinkObj {
//    func get (callback: Return<ProjectObj?>?) { Engine.getProject(id: id, callback: callback); } }
//class LinkTask : LinkObj {
//    func get (callback: Return<TaskObj?>?) { Engine.getTask(id: id, callback: callback); } }
//class LinkThread : LinkObj {
//    func get (callback: Return<ThreadObj?>?) { Engine.getThread(id: id, callback: callback); } }
//class LinkEvent : LinkObj {
//    func get (callback: Return<EventObj?>?) { Engine.getEvent(id: id, callback: callback); } }
//class LinkGroup : LinkObj {
//    func get (callback: Return<OrganisationObj?>?) {
//        Engine.getOrganisation(type: .interestGroup, id: id, callback: callback); } }
//class LinkOrganisation : LinkObj {
//    func get (callback: Return<OrganisationObj?>?) {
//        Engine.getOrganisation(type: .organisation, id: id, callback: callback); } }


















////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// BELOW THIS IS DEPRECATED ///////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


class LinksModel : BaseModel {
    
    //    var Links : UserLinks? { return UserLinks (json: self[(#function.snakeCase())]); }
    
    //
    //    override subscript (key: String)->AnyObject? {
    //        get { return LinkContent (json: dict?[key], parent: self) as AnyObject; }
    //        set { }
    //    }
    //
    //    var myself : LinkContent? { return self["self"] as? LinkContent; }
    ////        get { return LinkContent (json: self["self"], parent: self.parent) as AnyObject; }
    ////    }
    //
}

class LinkContent : BaseModel {
    
//    var href : String? {
//        if let href = dict?["href"] as? String { return updateLink(href); } else { return nil; }
//    }
    
//    private func updateLink(_ href: String?) -> String?{
//        guard var href = href else { return nil; }
//        href = href.replacingOccurrences(of: "/api", with: "")
//        href = href.replacingOccurrences(of: "?id=", with: "?key=")
//        return Url.getBaseUrl(appendUrl: href);
//    }
    
    //    func getModel (callback: ModelReturn? = nil) { Engine.getFromLink(url: href) { response in
    //        print ("GET MODEL FROM URL \(self.href ?? "nil") IS \(response?.arr?.summary() ?? "nil")");
    //        response?.parent = self.parent?.parent; callback?(response);
    //        }
    //    }
    //    func getImage (callback: ((UIImage?)->Void)? = nil) { Engine.requestImage(indirectUrl: href, callback: callback); }
}

