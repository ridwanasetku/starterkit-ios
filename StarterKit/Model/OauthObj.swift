//
//  OauthObj.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 27/01/22.
//

import Foundation

class LoginMetaObj: BaseModel {
    var message : String? { return self[#function.snake] as? String; }
    var token : String? { return self[#function.snake] as? String; }
  //  var accountNo : [Int]? { return self[#function.snake] as? [Int]; }
}

class accountObj: BaseModel {
    var message : String? { return self[#function.snake] as? String; }
}

class OAuthObj: BaseModel{
    var message : String? { return self[#function.snake] as? String; }
    var idUser: String? { return self["id"] as? String; }
  //  var refreshToken : String? { return self[#function.snake] as? String; }
    var accessToken : String? { return self[#function] as? String; }
    var data : String? { return self[#function] as? String; }
    var errorguys: String? { return self["error"] as? String; }
    var suksesguys: String? { return self["status"] as? String; }
}

class UserObj: BaseModel {
    var avatar : String? { return self[#function.snake] as? String; }
    var email : String? { return self[#function.snake] as? String; }
    var name : String? { return self[#function.snake] as? String; }
    var phone : String? { return self[#function.snake] as? String; }
}

class MetaObj: BaseModel {
    var code : Int? { return self[#function.snake] as? Int; }
    var message: String? { return self[#function.snake] as? String; }
}
