//
//  ProfileObj.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 23/02/22.
//

import Foundation

class ProfileModel: BaseModel {
    var content: ProfileObjs? { return ProfileObjs(self[#function]);}
}

class ProfileObj: BaseModel {
    var username : String? { return self[#function.snake] as? String; }
    var fullname : String? { return self[#function.snake] as? String; }
    var email : String? { return self[#function.snake] as? String; }
    var role : String? { return self[#function.snake] as? String; }
    var status : Bool? { return self[#function.snake] as? Bool; }
    
}

class ProfileObjs: BaseModels {
    typealias Single = ProfileObj;
    override subscript (key: Int?)->Single? { return Single (super[key]); }
    override func get(id: Int?)->Single? { return Single(super.get(id: id)); }
    override func get(idx: Int?)->Single? { return Single(super.get(idx: idx)); }
    func change(id: Int?, changed: Change<Single?>?) { set(id: id, newValue: changed?(get(id: id))); }
    func change(idx: Int?, changed: Change<Single?>?) { set(idx: idx, newValue: changed?(get(idx: idx))); }
}
