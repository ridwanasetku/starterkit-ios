//
//  VerifyObj.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 24/02/22.
//

import Foundation

class VerifyModel: BaseModel {
    var status : String? { return self[#function.snake] as? String; }
    var data: VerifyObjs? { return VerifyObjs(self[#function]);}
}

class VerifyObj: BaseModel {
    
    var nik : String? { return self[#function.snake] as? String; }
    var name : String? { return self[#function.snake] as? String; }
    var nameStatus : Bool? { return self[#function] as? Bool; }
    var birthdate : String? { return self[#function.snake] as? String; }
    var birthdateStatus : Bool? { return self[#function] as? Bool; }
    var birthplace : String? { return self[#function.snake] as? String; }
    var birthplaceStatus : Bool? { return self[#function] as? Bool; }
    var address : String? { return self[#function.snake] as? String; }
    var addressStatus : String? { return self[#function] as? String; }
    var createdAt : String? { return self[#function] as? String; }
    var roleName : String? { return self[#function] as? String; }
    var createdBy : Int? { return self[#function] as? Int; }
}

class VerifyObjs: BaseModels {
    typealias Single = VerifyObj;
    override subscript (key: Int?)->Single? { return Single (super[key]); }
    override func get(id: Int?)->Single? { return Single(super.get(id: id)); }
    override func get(idx: Int?)->Single? { return Single(super.get(idx: idx)); }
    func change(id: Int?, changed: Change<Single?>?) { set(id: id, newValue: changed?(get(id: id))); }
    func change(idx: Int?, changed: Change<Single?>?) { set(idx: idx, newValue: changed?(get(idx: idx))); }
}
