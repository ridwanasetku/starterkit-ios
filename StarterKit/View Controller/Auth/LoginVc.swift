//
//  LoginVc.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 04/02/22.
//

import Foundation
import UIKit
import MaterialComponents
import SwiftyRSA
import LocalAuthentication
import EllipticCurveKeyPair

class LoginVc: BaseViewController {
    
    //MARK: -RSA

    struct KeyPair {
        static let manager: EllipticCurveKeyPair.Manager = {
            let publicAccessControl = EllipticCurveKeyPair.AccessControl(protection: kSecAttrAccessibleAlwaysThisDeviceOnly, flags: [])
            let privateAccessControl = EllipticCurveKeyPair.AccessControl(protection: kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly, flags: [.userPresence, .privateKeyUsage])
            let config = EllipticCurveKeyPair.Config(
                publicLabel: "payment.sign.public",
                privateLabel: "payment.sign.private",
                operationPrompt: "Confirm payment",
                publicKeyAccessControl: publicAccessControl,
                privateKeyAccessControl: privateAccessControl,
                token: .secureEnclave)
            return EllipticCurveKeyPair.Manager(config: config)
        }()
    }
    
    //MARK: - Properties
    @IBOutlet weak var tfUsername: MDCOutlinedTextField!
    @IBOutlet weak var tfPassword: MDCOutlinedTextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    
    var publik = locker.ieuPublic
    var challenge: String?
    var signatureHasil: String?
    var signatureDefault = locker.signature
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        locker.oauth?.clearData()
       // imgFrame.transform = CGAffineTransform(scaleX: -1, y: 1)
       // btnBack.underline()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navcon = self.navigationController
        navcon?.setNavigationBarHidden(true, animated: false)
        btnLogin.makeRoundedRect(withCornerRadius: 8)
        locker.oauth?.clearData()
      //  print("ini publickey: \(locker.oauth?.token)")
      //   print("ini publik: \(isPublik)")
    }
    
    //MARK: - Selectors
    
    @objc private func btnShowPasswordTapped(_ sender: UIButton){
        sender.isSelected.toggle()
        self.tfPassword.isSecureTextEntry = !sender.isSelected
    }
    
    @objc private func tfEditingChanged(_ sender: MDCOutlinedTextField) {
//        if sender == tfUsername, let text = sender.text {
////            if text.count > 4, !(text.isValidEmail) {
////                //show error for some condition
////                sender.showError(message: "Not valid email")
////                return
////            }
//        }
        
        if sender.leadingAssistiveLabel.text != nil {
            //clear error
            sender.hideError(initalColor: .orange)
        }
    }
    
    @objc private func btnRegisterTapped(){
        RegisterOCRVc.synth.show(from: self)
    }
    
    @objc private func btnForgotPasswordTapped(){
     //   ForgotPasswordVc.synth.show(from: self)
    }
    
    @objc private func btnBackTapped(){
        //self.navigationController?.popViewController(animated: true)
        let vc = UIViewController.instantiate(named: "OnboardingVc") as! OnboardingVc
        vc.show(from: self)
    }
    
    
    @objc private func btnLoginTapped(){
        guard let username = tfUsername.text, !username.isEmpty else {
            tfUsername.showError(message: "Username is required")
            return
        }
        
        guard let password = tfPassword.text, !password.isEmpty else {
            tfPassword.showError(message: "Password is required")
            return
        }
        self.view.addIndicator()
        engine.login(username: tfUsername.text ?? "", password: tfPassword.text ?? ""){
            if $0?.isSuccess ?? false {
                locker.oauth = $0
              // print("ini tokennya : \(locker.oauth?.accessToken)")
                self.view.removeIndicator()
                DispatchQueue.main.async {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarVc")
                     //   UIApplication.shared.keyWindow?.rootViewController = viewController
                    UIApplication.shared.windows.first?.rootViewController = viewController
                    }
            }else {
                self.view.removeIndicator()
                toastUser("gagal")
            }
        }
        
//        print(" ini passwordnya : \(RSAManager.shared.getEncryptedPassword(from: password))")
      
    }
    
    //MARK: - Helpers
    
    private func configureUI() {
        self.view.backgroundColor = .white
        self.tfUsername.setAttribute(placeholder: "Username", color: .orange)
        self.tfUsername.addTarget(self, action: #selector(tfEditingChanged), for: .editingChanged)
        
        
        self.tfPassword.setAttribute(placeholder: "Password", color: .orange, isSecured: true)
        self.tfPassword.addTarget(self, action: #selector(tfEditingChanged), for: .editingChanged)
        
        let btnPassword = UIButton(type: .custom)
        btnPassword.frame = CGRect(x: CGFloat(0), y: CGFloat(0),
                                      width: CGFloat(tfPassword.frame.height), height: CGFloat(tfPassword.frame.height))
        btnPassword.setImage(UIImage(named: "visibility"), for: .normal)
        btnPassword.setImage(UIImage(named: "visibility_off"), for: .selected)
        btnPassword.addTarget(self, action: #selector(btnShowPasswordTapped), for: .touchUpInside)
        btnPassword.tintColor = .lightGray
        
        self.tfPassword.trailingView = btnPassword
        self.tfPassword.trailingViewMode = .always
        self.btnLogin.addTarget(self, action: #selector(btnLoginTapped), for: .touchUpInside)
        self.btnRegister.addTarget(self, action: #selector(btnRegisterTapped), for: .touchUpInside)
        self.btnForgotPassword.addTarget(self, action: #selector(btnForgotPasswordTapped), for: .touchUpInside)
        self.btnBack.addTarget(self, action: #selector(btnBackTapped), for: .touchUpInside)
    }
}

extension LoginVc {
    static var synth : LoginVc { return UIViewController.instantiate(named: "LoginVc") as! LoginVc}
    
    @discardableResult
    func `init` (containerVc: UIViewController? = nil) -> LoginVc {
         //self.containerVc = containerVc;
         return self;
     }
    
//    @discardableResult
//    func show (from vc: UIViewController?, email: String?) -> OtpVc {
//        self.emailLogin = email
//        super.show(from: vc)
//        return self;
//    }
}
