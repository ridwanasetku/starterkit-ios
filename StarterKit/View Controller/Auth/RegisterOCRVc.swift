//
//  RegisterOCRVc.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 06/03/22.
//

import Foundation
import UIKit
import SwiftOCR
import YPImagePicker
import Vision

class RegisterOCRVc: BaseViewController {
    
    @IBOutlet weak var imgKtp: UIImageView!
    @IBOutlet weak var tfNama: UILabel!
    
    var selectedImage:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func recognizeText(image: UIImage?){
        guard let cgImage = image?.cgImage else {
            fatalError("could not get cg image")
        }
        
        //Handler
        let handler = VNImageRequestHandler(cgImage: cgImage, options: [:])
        
        //Request
        let request = VNRecognizeTextRequest { [weak self] request, error in
            guard let observations = request.results as? [VNRecognizedTextObservation],
                  error == nil else {
                      return
                  }
            
            let text = observations.compactMap({
                $0.topCandidates(1).first?.string
            }).joined(separator: ", ")
            
            DispatchQueue.main.async {
                self?.tfNama.text = text
            }
       
        }
        
        // Process request
        do {
            try handler.perform([request])
        }catch {
            tfNama.text = "\(error)"
        }
    }
    
    @IBAction func btnPhotoTapped(_ sender: Any) {
        var config = YPImagePickerConfiguration()
        config.usesFrontCamera = true
        config.hidesStatusBar = false
        //config.library.defaultMultipleSelection = true
        let picker = YPImagePicker(configuration: config)
        picker.didFinishPicking { [unowned picker] items, _ in
            if let photo = items.singlePhoto {
                self.selectedImage = photo.image
                self.imgKtp.image = self.selectedImage
                
                self.recognizeText(image: self.selectedImage)
//                let swiftOCRInstance = SwiftOCR()
//                swiftOCRInstance.recognize(self.selectedImage!){ result in
//                    self.tfNama.text = result
//                    print("ini result: \(result)")
//                }
            }
            picker.dismiss(animated: true, completion: nil)
        }
        present(picker, animated: true, completion: nil)
    }
}


extension RegisterOCRVc {
    static var synth : RegisterOCRVc { return UIViewController.instantiate(named: "RegisterOCRVc") as! RegisterOCRVc}
    
    @discardableResult
    func `init` (containerVc: UIViewController? = nil) -> RegisterOCRVc {
         //self.containerVc = containerVc;
         return self;
     }
    
//    @discardableResult
//    func show (from vc: UIViewController?, id: Int?) -> SearchVc {
//        self.idCheck = id
//        super.show(from: vc)
//        return self;
//    }
}
