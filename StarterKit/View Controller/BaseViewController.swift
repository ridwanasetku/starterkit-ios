//
//  BaseViewController.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 03/02/22.
//

import Foundation
import UIKit
import FittedSheets

class BaseViewController: UIViewController {
    
   // var sheetVc: SheetViewController?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        var isRoot = false
        if let vc = self.navigationController?.viewControllers[0], vc == self {
            isRoot = true
        }
        let navCon = self.navigationController
        navCon?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.tabBarController?.tabBar.isHidden = !isRoot
    }
    
//    func presentInSheet(from: UIViewController){
//        self.sheetVc = SheetViewController(controller: self)
//        sheetVc?.extendBackgroundBehindHandle = true
//        sheetVc?.topCornersRadius = 10
//        from.present(sheetVc!, animated: true, completion: nil)
//    }
    
}

