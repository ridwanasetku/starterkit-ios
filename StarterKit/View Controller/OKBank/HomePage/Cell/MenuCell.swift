//
//  MenuCell.swift
//  StarterKit
//
//  Created by Stella Patricia on 24/03/22.
//

import Foundation
import UIKit

class MenuCell: UICollectionViewCell {
    static let identifier: String = String(describing: MenuCell.self)

    var imageView: UIImageView = {
        let imgView = UIImageView()
        
        imgView.image = UIImage(named: "home_filled")
        imgView.contentMode = .scaleToFill
        imgView.clipsToBounds = true
        
        return imgView
    }()
    var label: UILabel = {
        let lbl = UILabel()
        
        lbl.text = "Home"
        lbl.font = .systemFont(ofSize: 14)

        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        self.contentView.addSubview(self.imageView)
        self.contentView.addSubview(self.label)
        
        self.imageView.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(self.contentView).inset(4)
            $0.height.width.equalTo(52)
        }
        self.label.snp.makeConstraints {
            $0.top.equalTo(self.imageView.snp.bottom).offset(4)
            $0.centerX.equalTo(self.imageView)
            $0.bottom.equalTo(self.contentView).offset(-4)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
