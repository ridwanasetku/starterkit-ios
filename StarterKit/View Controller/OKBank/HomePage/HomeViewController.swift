//
//  AccountLoginViewController.swift
//  StarterKit
//
//  Created by Stella Patricia on 11/03/22.
//

import Foundation
import UIKit
import MaterialComponents
import SnapKit
import SideMenuSwift

class HomeViewController: BaseViewController {    
    var backgroundImgView: UIImageView = {
        let imgView = UIImageView()
        
        imgView.image = UIImage(named: "img_background")
        imgView.contentMode = .scaleAspectFill
        return imgView
    }()
    var scrollView: UIScrollView = {
        let view = UIScrollView()
        
        return view
    }()
    var collectionView: UICollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        self.setCollectionView()
        self.addSubviews()
        self.makeConstraints()
        self.setSideMenu()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}

extension HomeViewController {
    func addSubviews() {
        self.view.addSubview(self.backgroundImgView)
        self.view.addSubview(self.scrollView)
        
        self.scrollView.addSubview(self.collectionView ?? UICollectionView())
    }
    
    func makeConstraints() {
        self.backgroundImgView.snp.makeConstraints {
            $0.leading.trailing.top.equalTo(self.view.safeAreaLayoutGuide)
        }
        self.scrollView.snp.makeConstraints {
            $0.leading.trailing.top.bottom.equalTo(self.view.safeAreaLayoutGuide)
        }
        self.collectionView?.snp.makeConstraints {
            $0.top.bottom.equalTo(self.scrollView).inset(24)
            $0.leading.trailing.equalTo(self.view.safeAreaLayoutGuide).inset(24)
        }
    }
    
    func setCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 60, height: 60)
        
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        collectionView?.backgroundColor = .white
        
//        collectionView?.layer.masksToBounds = false
//        collectionView?.layer.cornerRadius = 10
//        collectionView?.layer.shadowColor = UIColor.gray.cgColor
//        collectionView?.layer.shadowOffset = CGSize(width: 0, height: 0)
//        collectionView?.layer.shadowOpacity = 0.7
//        collectionView?.layer.shadowRadius = 4.0
        
        collectionView?.register(MenuCell.self, forCellWithReuseIdentifier: MenuCell.identifier)
        
        collectionView?.reloadData()
    }
    
    func setSideMenu() {
        self.sideMenuController?.revealMenu()
        SideMenuController.preferences.basic.menuWidth = 240
        SideMenuController.preferences.basic.statusBarBehavior = .hideOnMenu
        SideMenuController.preferences.basic.position = .under
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        SideMenuController.preferences.basic.supportedOrientations = .portrait
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
    }
}

extension HomeViewController: SideMenuControllerDelegate {
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuCell.identifier, for: indexPath) as? MenuCell else { return UICollectionViewCell() }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
    
}
