//
//  LoginLenderInteractor.swift
//  StarterKit
//
//  Created by Stella Patricia on 22/03/22.
//

import Foundation
import Alamofire
import ObjectMapper

class LoginLenderInteractor: LoginLenderPresenterToInteractorProtocol{
    var presenter: LoginLenderInteractorToPresenterProtocol?
    
    func loginAccount(data: LoginLenderModel) {
        engine.loginOKBank(email: data.email ?? "",
                           password: data.password ?? "") {
            if $0?.isSuccess ?? false {
                self.presenter?.loginSuccess()
            }else {
                self.presenter?.loginFailed()
            }
        }
    }
}
