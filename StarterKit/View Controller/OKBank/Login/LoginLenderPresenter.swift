//
//  LoginLenderPresenter.swift
//  StarterKit
//
//  Created by Stella Patricia on 22/03/22.
//

import Foundation
import UIKit

class LoginLenderPresenter: LoginLenderViewToPresenterProtocol {
    
    var view: LoginLenderPresenterToViewProtocol?
    var interactor: LoginLenderPresenterToInteractorProtocol?
    var router: LoginLenderPresenterToRouterProtocol?
    
    
    func startLoginAccount(data: LoginLenderModel) {
        interactor?.loginAccount(data: data)
    }
}

extension LoginLenderPresenter: LoginLenderInteractorToPresenterProtocol{
    func loginSuccess() {
        view?.successToLogin()
    }
    
    func loginFailed() {
        view?.failedToLogin()
    }
}
