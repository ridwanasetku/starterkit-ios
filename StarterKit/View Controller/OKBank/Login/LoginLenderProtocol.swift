//
//  LoginLenderProtocol.swift
//  StarterKit
//
//  Created by Stella Patricia on 22/03/22.
//

import Foundation
import UIKit

protocol LoginLenderViewToPresenterProtocol: AnyObject{
    
    var view: LoginLenderPresenterToViewProtocol? { get set }
    var interactor: LoginLenderPresenterToInteractorProtocol? { get set }
    var router: LoginLenderPresenterToRouterProtocol? { get set }
    func startLoginAccount(data: LoginLenderModel)
}

protocol LoginLenderPresenterToViewProtocol: AnyObject{
    func successToLogin()
    func failedToLogin()
}

protocol LoginLenderPresenterToRouterProtocol: AnyObject {
    static func initPresenter(vc: LoginLenderVc) -> LoginLenderViewToPresenterProtocol

}

protocol LoginLenderPresenterToInteractorProtocol: AnyObject {
    var presenter: LoginLenderInteractorToPresenterProtocol? { get set }
    func loginAccount(data: LoginLenderModel)
}

protocol LoginLenderInteractorToPresenterProtocol: AnyObject {
    func loginSuccess()
    func loginFailed()
}
