//
//  LoginLenderRouter.swift
//  StarterKit
//
//  Created by Stella Patricia on 22/03/22.
//

import Foundation
import UIKit

class LoginLenderRouter: LoginLenderPresenterToRouterProtocol {
    static func initPresenter(vc: LoginLenderVc) -> LoginLenderViewToPresenterProtocol {
        let presenter: LoginLenderViewToPresenterProtocol & LoginLenderInteractorToPresenterProtocol = LoginLenderPresenter()
        let interactor: LoginLenderPresenterToInteractorProtocol = LoginLenderInteractor()
        let router: LoginLenderPresenterToRouterProtocol = LoginLenderRouter()
        
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        return presenter
    }
}
