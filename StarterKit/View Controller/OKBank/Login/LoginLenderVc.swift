//
//  LoginLenderVc.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 14/02/22.
//

import Foundation
import UIKit
import MaterialComponents
import SwiftyRSA
import LocalAuthentication
import EllipticCurveKeyPair

class LoginLenderVc: BaseViewController {
    
    //MARK: -RSA

    struct KeyPair {
        static let manager: EllipticCurveKeyPair.Manager = {
            let publicAccessControl = EllipticCurveKeyPair.AccessControl(protection: kSecAttrAccessibleAlwaysThisDeviceOnly, flags: [])
            let privateAccessControl = EllipticCurveKeyPair.AccessControl(protection: kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly, flags: [.userPresence, .privateKeyUsage])
            let config = EllipticCurveKeyPair.Config(
                publicLabel: "payment.sign.public",
                privateLabel: "payment.sign.private",
                operationPrompt: "Confirm payment",
                publicKeyAccessControl: publicAccessControl,
                privateKeyAccessControl: privateAccessControl,
                token: .secureEnclave)
            return EllipticCurveKeyPair.Manager(config: config)
        }()
    }
    
    //MARK: - Properties
    @IBOutlet weak var imgFrame: UIImageView!
    @IBOutlet weak var tfUsername: MDCOutlinedTextField!
    @IBOutlet weak var tfPassword: MDCOutlinedTextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnFingerPrint: UIButton!
    @IBOutlet weak var btnForgotUsername: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnFingerprint: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var lblAtau: UILabel!
    @IBOutlet weak var lblTidakPunya: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    var presenter: LoginLenderViewToPresenterProtocol?

    var publik = locker.ieuPublic
    var challenge: String?
    var signatureHasil: String?
    var signatureDefault = locker.signature
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        locker.oauth?.clearData()
       // imgFrame.transform = CGAffineTransform(scaleX: -1, y: 1)
       // btnBack.underline()
        
        presenter = LoginLenderRouter.initPresenter(vc: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let navcon = self.navigationController
        navcon?.setNavigationBarHidden(true, animated: false)
        btnLogin.makeRoundedRect(withCornerRadius: 8)
        locker.oauth?.clearData()
      //  print("ini publickey: \(locker.oauth?.token)")
        print("ini publik: \(isPublik)")
    }
    
    //MARK: - Selectors
    
    @objc private func btnShowPasswordTapped(_ sender: UIButton){
        sender.isSelected.toggle()
        self.tfPassword.isSecureTextEntry = !sender.isSelected
    }
    
    @objc private func tfEditingChanged(_ sender: MDCOutlinedTextField) {
//        if sender == tfUsername, let text = sender.text {
////            if text.count > 4, !(text.isValidEmail) {
////                //show error for some condition
////                sender.showError(message: "Not valid email")
////                return
////            }
//        }
        
        if sender.leadingAssistiveLabel.text != nil {
            //clear error
            sender.hideError(initalColor: .orange)
        }
    }
    
    @objc private func btnRegisterTapped(){
      //  RegisterVc.synth.show(from: self)
        let vc = AccountRegisterViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func btnForgotUsernameTapped(){
       // ForgotUsernameVc.synth.show(from: self)
    }
    
    @objc private func btnForgotPasswordTapped(){
     //   ForgotPasswordVc.synth.show(from: self)
    }
    
    @objc private func btnFingerTapped(){


    }
    
    @objc private func btnBackTapped(){
        //self.navigationController?.popViewController(animated: true)
        let vc = UIViewController.instantiate(named: "OnboardingVc") as! OnboardingVc
        vc.show(from: self)
    }
    
    
    @objc private func btnLoginTapped(){
        
//        guard let username = tfUsername.text, !username.isEmpty else {
//            tfUsername.showError(message: "Username is required")
//            return
//        }
//
//        guard let password = tfPassword.text, !password.isEmpty else {
//            tfPassword.showError(message: "Password is required")
//            return
//        }
//
//        if tfUsername.text == "aobaa" && tfPassword.text == "passwordVal" {
//            toastUser("Login Sukses ", color: .greenKalteng, duration: 3)
//        }else{
//            toastUser("Login Gagal", color: .red, duration: 3)
//        }
//
//        print(" ini passwordnya : \(RSAManager.shared.getEncryptedPassword(from: password))")
        
        let data = LoginLenderModel()
        data.email = tfUsername.text
        let password = tfPassword.text ?? ""
        data.password = RSAManager.shared.getEncryptedPassword(from: password)
//        presenter?.startLoginAccount(data: data)
        let vc = HomeViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - Helpers
    
    private func configureUI() {
        self.containerView.layer.masksToBounds = false
        self.containerView.layer.cornerRadius = 10
        self.containerView.layer.shadowColor = UIColor.gray.cgColor
        self.containerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.containerView.layer.shadowOpacity = 0.7
        self.containerView.layer.shadowRadius = 4.0

        self.view.backgroundColor = .white
        self.tfUsername.setAttribute(placeholder: "Username", color: .orange)
        self.tfUsername.addTarget(self, action: #selector(tfEditingChanged), for: .editingChanged)
        
        
        self.tfPassword.setAttribute(placeholder: "Password", color: .orange, isSecured: true)
        self.tfPassword.addTarget(self, action: #selector(tfEditingChanged), for: .editingChanged)
        
        let btnPassword = UIButton(type: .custom)
        btnPassword.frame = CGRect(x: CGFloat(0), y: CGFloat(0),
                                      width: CGFloat(tfPassword.frame.height), height: CGFloat(tfPassword.frame.height))
        btnPassword.setImage(UIImage(named: "visibility"), for: .normal)
        btnPassword.setImage(UIImage(named: "visibility_off"), for: .selected)
        btnPassword.addTarget(self, action: #selector(btnShowPasswordTapped), for: .touchUpInside)
        btnPassword.tintColor = .lightGray
        
        self.tfPassword.trailingView = btnPassword
        self.tfPassword.trailingViewMode = .always
        self.btnFingerprint.addTarget(self, action: #selector(btnFingerTapped), for: .touchUpInside)
        self.btnLogin.addTarget(self, action: #selector(btnLoginTapped), for: .touchUpInside)
        self.btnRegister.addTarget(self, action: #selector(btnRegisterTapped), for: .touchUpInside)
        self.btnForgotUsername.addTarget(self, action: #selector(btnForgotUsernameTapped), for: .touchUpInside)
        self.btnForgotPassword.addTarget(self, action: #selector(btnForgotPasswordTapped), for: .touchUpInside)
        self.btnBack.addTarget(self, action: #selector(btnBackTapped), for: .touchUpInside)
        
        if isEnglish == true {
            btnFingerprint.setTitle("USE BIOMETRICS", for: .normal)
            btnForgotUsername.setTitle("FORGOT USERNAME", for: .normal)
            btnForgotPassword.setTitle("FORGOT PASSWORD", for: .normal)
            btnRegister.setTitle("REGISTER", for: .normal)
            lblAtau.text = "Or"
            lblTidakPunya.text = "Did'nt have any account?"
        }else {
            btnFingerprint.setTitle("GUNAKAN BIOMETRICS", for: .normal)
            btnForgotUsername.setTitle("LUPA USERNAME", for: .normal)
            btnForgotPassword.setTitle("LUPA PASSWORD", for: .normal)
            btnRegister.setTitle("DAFTAR", for: .normal)
            lblAtau.text = "Atau"
            lblTidakPunya.text = "Tidak punya akun?"
        }
    }
}

extension LoginLenderVc {
    static var synth : LoginLenderVc { return UIViewController.instantiate(named: "LoginLenderVc") as! LoginLenderVc}
    
    @discardableResult
    func `init` (containerVc: UIViewController? = nil) -> LoginLenderVc {
         //self.containerVc = containerVc;
         return self;
     }
    
//    @discardableResult
//    func show (from vc: UIViewController?, email: String?) -> OtpVc {
//        self.emailLogin = email
//        super.show(from: vc)
//        return self;
//    }
}

extension LoginLenderVc: LoginLenderPresenterToViewProtocol {
    func successToLogin() {
        let vc = HomeViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func failedToLogin() {
        
    }
}
