//
//  RegisterValidateOTPInteractor.swift
//  StarterKit
//
//  Created by Stella Patricia on 22/03/22.
//

import Foundation
import Alamofire
import ObjectMapper

class RegisterValidateOTPInteractor: RegisterValidateOTPPresenterToInteractorProtocol{

    var presenter: RegisterValidateOTPInteractorToPresenterProtocol?
    
    func validateOTP(email: String, otp: String) {
        engine.registerValidateOTPOKBank(email: email, otp: otp) {
            if $0?.isSuccess ?? false {
                self.presenter?.validateOTPSuccess()
            }else {
                self.presenter?.validateOTPFailed()
            }
        }
    }
}
