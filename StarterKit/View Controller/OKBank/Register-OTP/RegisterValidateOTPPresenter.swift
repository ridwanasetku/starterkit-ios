//
//  RegisterValidateOTPPresenter.swift
//  StarterKit
//
//  Created by Stella Patricia on 22/03/22.
//

import Foundation
import UIKit

class RegisterValidateOTPPresenter: RegisterValidateOTPViewToPresenterProtocol {
    var view: RegisterValidateOTPPresenterToViewProtocol?
    var interactor: RegisterValidateOTPPresenterToInteractorProtocol?
    var router: RegisterValidateOTPPresenterToRouterProtocol?
    
    func startValidateOTP(email: String, otp: String) {
        interactor?.validateOTP(email: email, otp: otp)
    }
}

extension RegisterValidateOTPPresenter: RegisterValidateOTPInteractorToPresenterProtocol{
    func validateOTPSuccess() {
        view?.validateOTP()
    }
    
    func validateOTPFailed() {
        view?.validateOTPError()
    }
}
