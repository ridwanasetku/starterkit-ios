//
//  RegisterValidateOTPProtocol.swift
//  StarterKit
//
//  Created by Stella Patricia on 22/03/22.
//

import Foundation
import UIKit

protocol RegisterValidateOTPViewToPresenterProtocol: AnyObject{
    
    var view: RegisterValidateOTPPresenterToViewProtocol? { get set }
    var interactor: RegisterValidateOTPPresenterToInteractorProtocol? { get set }
    var router: RegisterValidateOTPPresenterToRouterProtocol? { get set }
    
    func startValidateOTP(email: String, otp: String)
}

protocol RegisterValidateOTPPresenterToViewProtocol: AnyObject{
    func validateOTP()
    func validateOTPError()
}

protocol RegisterValidateOTPPresenterToRouterProtocol: AnyObject {
    static func initPresenter(vc: RegisterValidateOTPViewController) -> RegisterValidateOTPViewToPresenterProtocol

}

protocol RegisterValidateOTPPresenterToInteractorProtocol: AnyObject {
    var presenter: RegisterValidateOTPInteractorToPresenterProtocol? { get set }
    
    func validateOTP(email: String, otp: String)
}

protocol RegisterValidateOTPInteractorToPresenterProtocol: AnyObject {
    func validateOTPSuccess()
    func validateOTPFailed()
}
