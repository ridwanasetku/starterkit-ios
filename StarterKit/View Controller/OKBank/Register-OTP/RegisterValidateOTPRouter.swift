//
//  RegisterValidateOTPRouter.swift
//  StarterKit
//
//  Created by Stella Patricia on 22/03/22.
//

import Foundation
import UIKit

class RegisterValidateOTPRouter: RegisterValidateOTPPresenterToRouterProtocol {
    static func initPresenter(vc: RegisterValidateOTPViewController) -> RegisterValidateOTPViewToPresenterProtocol {
        let presenter: RegisterValidateOTPViewToPresenterProtocol & RegisterValidateOTPInteractorToPresenterProtocol = RegisterValidateOTPPresenter()
        let interactor: RegisterValidateOTPPresenterToInteractorProtocol = RegisterValidateOTPInteractor()
        let router: RegisterValidateOTPPresenterToRouterProtocol = RegisterValidateOTPRouter()
        
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        return presenter
    }
}
