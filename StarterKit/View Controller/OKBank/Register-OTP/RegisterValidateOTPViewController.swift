//
//  RegisterValidateOTPViewController.swift
//  StarterKit
//
//  Created by Stella Patricia on 22/03/22.
//

import Foundation
import UIKit
import MaterialComponents
import SnapKit

class RegisterValidateOTPViewController: UIViewController {
    var containerView: UIView = {
        let view = UIView()
        
        view.backgroundColor = .white
        
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.7
        view.layer.shadowRadius = 4.0
        
        return view
    }()
    var backgroundImgView: UIImageView = {
        let imgView = UIImageView()
        
        imgView.image = UIImage(named: "img_background")
        imgView.contentMode = .scaleAspectFill
        return imgView
    }()
    public var otpTextField: MDCOutlinedTextField = {
        let tf = MDCOutlinedTextField()
        
        tf.font = .systemFont(ofSize: 14)
        tf.setAttribute(placeholder: "OTP", color: UIColor.orangeDefault)
        
        return tf
    }()
    public var validateButton: UIButton = {
        let button = UIButton()
        
        button.setTitle("VALIDATE", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 14)
        button.backgroundColor = UIColor.orangeDefault
        button.makeRoundedRect(withCornerRadius: 8)
        
        return button
    }()
    var backButton: UIButton = {
        let button = UIButton()
        
        button.setTitle("BACK", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = UIColor.yellowKalteng
        button.titleLabel?.font = .boldSystemFont(ofSize: 14)
        button.makeRoundedRect(withCornerRadius: 8)

        return button
    }()
    public var email: String?
    var presenter: RegisterValidateOTPViewToPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        self.addSubviews()
        self.makeConstraints()
        
        self.backButton.addTarget(self, action: #selector(tappedBackButton), for: .touchUpInside)
        self.validateButton.addTarget(self, action: #selector(tappedValidateButton), for: .touchUpInside)
        
        presenter = RegisterValidateOTPRouter.initPresenter(vc: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}

extension RegisterValidateOTPViewController {
    func addSubviews() {
        self.view.addSubview(self.backgroundImgView)
        self.view.addSubview(self.containerView)
        
        self.containerView.addSubview(self.otpTextField)
        self.containerView.addSubview(self.validateButton)
        self.containerView.addSubview(self.backButton)
    }
    
    func makeConstraints() {
        self.backgroundImgView.snp.makeConstraints {
            $0.leading.trailing.top.equalTo(self.view.safeAreaLayoutGuide)
        }
        self.containerView.snp.makeConstraints {
            $0.centerY.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalTo(self.view.safeAreaLayoutGuide).inset(24)
        }
        self.otpTextField.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(self.containerView).inset(24)
        }
        self.backButton.snp.makeConstraints {
            $0.top.equalTo(self.otpTextField.snp.bottom).offset(16)
            $0.bottom.equalTo(self.containerView).offset(-24)
            $0.leading.equalTo(self.containerView).offset(24)
            $0.height.equalTo(40)
        }
        self.validateButton.snp.makeConstraints {
            $0.top.equalTo(self.otpTextField.snp.bottom).offset(16)
            $0.bottom.equalTo(self.containerView).offset(-24)
            $0.leading.equalTo(self.backButton.snp.trailing).offset(16)
            $0.trailing.equalTo(self.containerView).offset(-24)
            $0.height.equalTo(40)
            $0.width.equalTo(self.backButton)
        }
    }
}

extension RegisterValidateOTPViewController {
    @objc func tappedBackButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func tappedValidateButton() {
        presenter?.startValidateOTP(email: email ?? "", otp: otpTextField.text ?? "")
    }
}

extension RegisterValidateOTPViewController: RegisterValidateOTPPresenterToViewProtocol {
    func validateOTP() {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoginLenderVc.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func validateOTPError() {
        
    }
}
