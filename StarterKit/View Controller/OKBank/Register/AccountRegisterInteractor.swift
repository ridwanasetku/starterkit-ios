//
//  AccountRegisterInteractor.swift
//  StarterKit
//
//  Created by Stella Patricia on 15/03/22.
//

import Foundation
import Alamofire
import ObjectMapper

class AccountRegisterInteractor: AccountRegisterPresenterToInteractorProtocol{
    var presenter: AccountRegisterInteractorToPresenterProtocol?

    func fetchData(data: AccountRegisterModel) {
        
        engine.registerOKBank(firstName: data.firstName ?? "",
                              lastName: data.lastName ?? "",
                              email: data.email ?? "",
                              password: data.password ?? "",
                              role: data.role ?? "",
                              group: data.group ?? "") {
            if $0?.isSuccess ?? false {
                self.presenter?.dataFetchedSuccess()
            }else {
                self.presenter?.dataFetchFailed()
            }
        }
    }
    
    func generateOTP(email: String) {
        engine.registerGenerateOTPOKBank(email: email ?? "") {
            if $0?.isSuccess ?? false {
                self.presenter?.generateOTPSuccess()
            }else {
                self.presenter?.generateOTPFailed()
            }
        }
    }
}
