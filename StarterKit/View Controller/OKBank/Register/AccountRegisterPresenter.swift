//
//  AccountRegisterPresenter.swift
//  StarterKit
//
//  Created by Stella Patricia on 15/03/22.
//

import Foundation
import UIKit

class AccountRegisterPresenter: AccountRegisterViewToPresenterProtocol {
    
    var view: AccountRegisterPresenterToViewProtocol?
    var interactor: AccountRegisterPresenterToInteractorProtocol?
    var router: AccountRegisterPresenterToRouterProtocol?
    
    func startFetchingData(data: AccountRegisterModel) {
        interactor?.fetchData(data: data)
    }
    
    func startGenerateOTP(email: String) {
        interactor?.generateOTP(email: email)
    }
}

extension AccountRegisterPresenter: AccountRegisterInteractorToPresenterProtocol{
    func dataFetchedSuccess() {
        view?.showData()
    }
    
    func dataFetchFailed() {
        view?.showError()
    }
    func generateOTPSuccess() {
        view?.generateOTP()
    }
    
    func generateOTPFailed() {
        view?.generateOTPError()
    }
}
