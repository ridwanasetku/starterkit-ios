//
//  AccountRegisterProtocol.swift
//  StarterKit
//
//  Created by Stella Patricia on 15/03/22.
//

import Foundation
import UIKit

protocol AccountRegisterViewToPresenterProtocol: AnyObject{
    
    var view: AccountRegisterPresenterToViewProtocol? { get set }
    var interactor: AccountRegisterPresenterToInteractorProtocol? { get set }
    var router: AccountRegisterPresenterToRouterProtocol? { get set }
    
    func startFetchingData(data: AccountRegisterModel)
    func startGenerateOTP(email: String)
}

protocol AccountRegisterPresenterToViewProtocol: AnyObject{
    func showData()
    func showError()
    
    func generateOTP()
    func generateOTPError()
}

protocol AccountRegisterPresenterToRouterProtocol: AnyObject {
    static func initPresenter(vc: AccountRegisterViewController) -> AccountRegisterViewToPresenterProtocol

}

protocol AccountRegisterPresenterToInteractorProtocol: AnyObject {
    var presenter: AccountRegisterInteractorToPresenterProtocol? { get set }
    
    func fetchData(data: AccountRegisterModel)
    
    func generateOTP(email: String)
}

protocol AccountRegisterInteractorToPresenterProtocol: AnyObject {
    func dataFetchedSuccess()
    func dataFetchFailed()
    
    func generateOTPSuccess()
    func generateOTPFailed()
}
