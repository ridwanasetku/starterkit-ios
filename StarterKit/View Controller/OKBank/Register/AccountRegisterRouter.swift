//
//  AccountRegisterRouter.swift
//  StarterKit
//
//  Created by Stella Patricia on 15/03/22.
//

import Foundation
import UIKit

class AccountRegisterRouter: AccountRegisterPresenterToRouterProtocol {
    static func initPresenter(vc: AccountRegisterViewController) -> AccountRegisterViewToPresenterProtocol {
        let presenter: AccountRegisterViewToPresenterProtocol & AccountRegisterInteractorToPresenterProtocol = AccountRegisterPresenter()
        let interactor: AccountRegisterPresenterToInteractorProtocol = AccountRegisterInteractor()
        let router: AccountRegisterPresenterToRouterProtocol = AccountRegisterRouter()
        
        presenter.view = vc
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        return presenter
    }
}
