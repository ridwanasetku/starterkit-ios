//
//  AccountRegisterViewController.swift
//  StarterKit
//
//  Created by Stella Patricia on 09/03/22.
//

import Foundation
import UIKit
import MaterialComponents
import SnapKit

class AccountRegisterViewController: BaseViewController {
    
    var containerView: UIView = {
        let view = UIView()
        
        view.backgroundColor = .white
        
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 10
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.7
        view.layer.shadowRadius = 4.0
        
        return view
    }()
    var backgroundImgView: UIImageView = {
        let imgView = UIImageView()
        
        imgView.image = UIImage(named: "img_background")
        imgView.contentMode = .scaleAspectFill
        return imgView
    }()
    public var firstNameTextField: MDCOutlinedTextField = {
        let tf = MDCOutlinedTextField()
        
        tf.font = .systemFont(ofSize: 14)
        tf.setAttribute(placeholder: "First Name", color: UIColor.orangeDefault)
        
        return tf
    }()
    public var lastNameTextField: MDCOutlinedTextField = {
        let tf = MDCOutlinedTextField()
        
        tf.font = .systemFont(ofSize: 14)
        tf.setAttribute(placeholder: "Last Name", color: UIColor.orangeDefault)
        
        return tf
    }()
    public var emailTextField: MDCOutlinedTextField = {
        let tf = MDCOutlinedTextField()
        
        tf.font = .systemFont(ofSize: 14)
        tf.setAttribute(placeholder: "Email", color: UIColor.orangeDefault)
        
        return tf
    }()
    public var passwordTextField: MDCOutlinedTextField = {
        let tf = MDCOutlinedTextField()
        
        tf.font = .systemFont(ofSize: 14)
        tf.setAttribute(placeholder: "Password", color: UIColor.orangeDefault)
        
        return tf
    }()
    public var roleTextField: MDCOutlinedTextField = {
        let tf = MDCOutlinedTextField()
        
        tf.font = .systemFont(ofSize: 14)
        tf.setAttribute(placeholder: "Role", color: UIColor.orangeDefault)
        
        return tf
    }()
    public var groupTextField: MDCOutlinedTextField = {
        let tf = MDCOutlinedTextField()
        
        tf.font = .systemFont(ofSize: 14)
        tf.setAttribute(placeholder: "Group", color: UIColor.orangeDefault)
        
        return tf
    }()
    public var checkBox: UIButton = {
        let button = UIButton()
        
         /* checkmark.square & square */
        button.setImage(UIImage(systemName: "square"), for: .normal)
        button.tintColor = UIColor.orangeDefault
        
        return button
    }()
    public var label: UILabel = {
        let lbl = UILabel()
        
        lbl.text = "Agree with Terms & Conditions"
        lbl.textColor = UIColor.orangeDefault
        lbl.font = .systemFont(ofSize: 12)
        
        return lbl
    }()
    public var registerButton: UIButton = {
        let button = UIButton()
        
        button.setTitle("SIGNUP", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 14)
        button.backgroundColor = UIColor.orangeDefault
        button.makeRoundedRect(withCornerRadius: 8)
        
        return button
    }()
    var backButton: UIButton = {
        let button = UIButton()
        
        button.setTitle("BACK", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = UIColor.yellowKalteng
        button.titleLabel?.font = .boldSystemFont(ofSize: 14)
        button.makeRoundedRect(withCornerRadius: 8)

        return button
    }()
    var presenter: AccountRegisterViewToPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        self.addSubviews()
        self.makeConstraints()
        
        self.backButton.addTarget(self, action: #selector(tappedBackButton), for: .touchUpInside)
        self.registerButton.addTarget(self, action: #selector(tappedSignUpButton), for: .touchUpInside)
        
        presenter = AccountRegisterRouter.initPresenter(vc: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}

extension AccountRegisterViewController {
    func addSubviews() {
        self.view.addSubview(self.backgroundImgView)
        self.view.addSubview(self.containerView)
        
        self.containerView.addSubview(self.firstNameTextField)
        self.containerView.addSubview(self.lastNameTextField)
        self.containerView.addSubview(self.emailTextField)
        self.containerView.addSubview(self.passwordTextField)
        self.containerView.addSubview(self.roleTextField)
        self.containerView.addSubview(self.groupTextField)
        self.containerView.addSubview(self.checkBox)
        self.containerView.addSubview(self.label)
        self.containerView.addSubview(self.registerButton)
        self.containerView.addSubview(self.backButton)
    }
    
    func makeConstraints() {
        self.backgroundImgView.snp.makeConstraints {
            $0.leading.trailing.top.equalTo(self.view.safeAreaLayoutGuide)
        }
        self.containerView.snp.makeConstraints {
            $0.centerY.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalTo(self.view.safeAreaLayoutGuide).inset(24)
        }
        self.firstNameTextField.snp.makeConstraints {
            $0.top.leading.trailing.equalTo(self.containerView).inset(24)
        }
        self.lastNameTextField.snp.makeConstraints {
            $0.top.equalTo(self.firstNameTextField.snp.bottom).offset(16)
            $0.leading.trailing.equalTo(self.containerView).inset(24)
        }
        self.emailTextField.snp.makeConstraints {
            $0.top.equalTo(self.lastNameTextField.snp.bottom).offset(16)
            $0.leading.trailing.equalTo(self.containerView).inset(24)
        }
        self.passwordTextField.snp.makeConstraints {
            $0.top.equalTo(self.emailTextField.snp.bottom).offset(16)
            $0.leading.trailing.equalTo(self.containerView).inset(24)
        }
        self.roleTextField.snp.makeConstraints {
            $0.top.equalTo(self.passwordTextField.snp.bottom).offset(16)
            $0.leading.trailing.equalTo(self.containerView).inset(24)
        }
        self.groupTextField.snp.makeConstraints {
            $0.top.equalTo(self.roleTextField.snp.bottom).offset(16)
            $0.leading.trailing.equalTo(self.containerView).inset(24)
        }
        self.checkBox.snp.makeConstraints {
            $0.leading.equalTo(self.containerView).offset(24)
            $0.top.equalTo(self.groupTextField.snp.bottom).offset(16)
            $0.height.width.equalTo(20)
        }
        self.label.snp.makeConstraints {
            $0.centerY.equalTo(self.checkBox)
            $0.leading.equalTo(self.checkBox.snp.trailing).offset(4)
            $0.trailing.equalTo(self.containerView).offset(-24)
        }
        self.backButton.snp.makeConstraints {
            $0.top.equalTo(self.checkBox.snp.bottom).offset(16)
            $0.bottom.equalTo(self.containerView).offset(-24)
            $0.leading.equalTo(self.containerView).offset(24)
            $0.height.equalTo(40)
        }
        self.registerButton.snp.makeConstraints {
            $0.top.equalTo(self.checkBox.snp.bottom).offset(16)
            $0.bottom.equalTo(self.containerView).offset(-24)
            $0.leading.equalTo(self.backButton.snp.trailing).offset(16)
            $0.trailing.equalTo(self.containerView).offset(-24)
            $0.height.equalTo(40)
            $0.width.equalTo(self.backButton)
        }
    }
}

extension AccountRegisterViewController {
    @objc func tappedBackButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func tappedSignUpButton() {
        let data = AccountRegisterModel()
        data.firstName = firstNameTextField.text
        data.lastName = lastNameTextField.text
        data.email = emailTextField.text
        let password = passwordTextField.text ?? ""
        data.password = RSAManager.shared.getEncryptedPassword(from: password)
        data.role = roleTextField.text
        data.group = groupTextField.text
        presenter?.startFetchingData(data: data)
    }
}

extension AccountRegisterViewController: AccountRegisterPresenterToViewProtocol {
    func showData() {
        presenter?.startGenerateOTP(email: emailTextField.text ?? "")
    }
    
    func showError() {
    }
    
    func generateOTP() {
        let vc = RegisterValidateOTPViewController()
        vc.email = emailTextField.text
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func generateOTPError() {
    }
}
