//
//  OnBoardingVc.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 03/02/22.
//

import Foundation
import UIKit
import FSPagerView

class OnboardingVc: BaseViewController {
    //MARK: - Properties
    @IBOutlet weak var btnLanguange: UIButton!
    @IBOutlet weak var btnLakuPandai: UIButton!
    @IBOutlet weak var carouselView: FSPagerView!
    private let dataImage: [UIImage?] = [
        UIImage(named: "asetku"),
        UIImage(named: "okbank")
    ]
    
    private let labelProduk : [String] = ["ASLI RI","OK BANK"]
    private let backgroundColor: [UIColor] = [.systemTeal, .systemIndigo]
    
    var language = false
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        isEnglish = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //locker.oauth?.clear()
        let navCon = self.navigationController
        navCon?.setNavigationBarHidden(true, animated: false)
    }
    
    
    //MARK: - Selectors
//    @IBAction func btnLanguageTapped(_ sender: Any) {
//        if (language == false){
//            isEnglish = false
//            btnLanguange.setImage(UIImage(named: "ic_lang_in"), for: .normal)
//        }else{
//            isEnglish = true
//            btnLanguange.setImage(UIImage(named: "ic_lang_en"), for: .normal)
//        }
//        language = !language
//    }
    
    
    //MARK: - Helpers
    
    private func configureUI() {
       // self.view.backgroundColor = backgroundColor.first
        self.view.backgroundColor = .white
        self.carouselView.backgroundColor = .clear
        self.carouselView.delegate = self
        self.carouselView.dataSource = self
        self.carouselView.register(PagerViewCustomImageCell.self, forCellWithReuseIdentifier: "PagerViewCustomImageCell")
        self.carouselView.isInfinite = true
        
        let height = self.carouselView.frame.height
        self.carouselView.itemSize = CGSize(width: height * 1.06, height: height)
        self.carouselView.transformer = FSPagerViewTransformer(type: .linear)
        self.carouselView.transformer?.minimumScale = 0.9
        self.carouselView.transformer?.minimumAlpha = 1
    }
}

//MARK: FSPager Delegate + DataSource
extension OnboardingVc: FSPagerViewDelegate, FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return dataImage.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "PagerViewCustomImageCell", at: index) as! PagerViewCustomImageCell
        cell.setup(image: dataImage[index])
        cell.setupLabel(label: labelProduk[index])
        cell.contentView.noShadow()
        cell.contentView.backgroundColor = .clear
        cell.backgroundColor = .clear
        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: false)
        print("DEBUG SELECTED INDEX", index)
        switch index {
        case 0:
//            let vc = UIViewController.instantiate(named: "HomeVc") as! HomeVc
//            vc.show(from: self)
            LoginVc.synth.show(from: self)
         //   isGuest = true
        case 1:
            LoginLenderVc.synth.show(from: self)
           // HomeDigiBroVc.synth.show(from: self)
            
        default:
            let vc = UIViewController.instantiate(named: "LoginVc") as! LoginVc
            self.present(vc, animated: true, completion: nil)
        }
    }
    
//    func pagerViewDidEndDecelerating(_ pagerView: FSPagerView) {
//        let index = pagerView.currentIndex
//        UIView.animate(withDuration: 0.1) {[weak self] in
//            guard let self = self else { return }
//            self.view.backgroundColor = self.backgroundColor[index]
//        }
//    }
}
