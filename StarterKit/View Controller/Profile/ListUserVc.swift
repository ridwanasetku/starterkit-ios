//
//  ListUserVc.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 25/02/22.
//

import Foundation
import UIKit

class ListUserVc: BaseViewController {
    
    @IBOutlet weak var cv: UICollectionView!
    
    var dataUser: ProfileObjs? { didSet { cv.reloadData()}}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cv.backgroundColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadData()
    }
    
    func reloadData(){
        self.view.addIndicator()
        engine.getListUser(page: 0, size: 100){
            if $0?.isSuccess ?? false {
                self.view.removeIndicator()
                self.dataUser = $0?.content
            }else {
                self.view.removeIndicator()
                toastUser("Gagal Load Data", color: .red, duration: 3.0)
            }
        }
    }
}

extension ListUserVc : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataUser?.count ?? 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let viewUtama = cell.view(100)
        let name = cell.label(1)
        let email = cell.label(2)
        let role = cell.label(3)
        let gambar = cell.image(10)
        let data = dataUser?[indexPath.row]
        name?.text = data?.fullname
        email?.text = data?.email
        role?.text = data?.role
        gambar?.makeRounded()
        //Uncomment this menu
//        if data?.avatar?.url == "" {
//            gambar?.image = #imageLiteral(resourceName: "ic_akun_active")
//        }else {
//            data?.avatar?.getImage(){
//                gambar?.image = $0
//            }
//        }
 
        viewUtama?.setborderGold()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cv.width, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        toastUser("Under Development", color: .red, duration: 3.0)
    }
}

extension ListUserVc{
    static var synth : ListUserVc { return UIViewController.instantiate(named: "ListUserVc") as! ListUserVc}
    
    @discardableResult
    func `init` (containerVc: UIViewController? = nil) -> ListUserVc {
         //self.containerVc = containerVc;
         return self;
     }
    
//    @discardableResult
//    func show (from vc: UIViewController?, email: String?) -> OtpVc {
//        self.emailLogin = email
//        super.show(from: vc)
//        return self;
//    }
}

