//
//  ProfileVc.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 22/02/22.
//

import Foundation
import UIKit

class ProfileVc: BaseViewController {
    
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var viewGradasi: UIView!
    @IBOutlet weak var viewListUser: UIView!
    @IBOutlet weak var viewResetPassword: UIView!
    @IBOutlet weak var viewLogout: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgProfile.makeRounded()
        viewListUser.setShadow(x: 0.5, y: 0.5, r: 5, a: 0.4, c: .darkGray)
        viewResetPassword.setShadow(x: 0.5, y: 0.5, r: 5, a: 0.4, c: .darkGray)
        viewLogout.setShadow(x: 0.5, y: 0.5, r: 5, a: 0.4, c: .darkGray)
  
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let navCon = self.navigationController
        navCon?.setNavigationBarHidden(true, animated: false)
        reloadData()
        setGradientBackground()
    }
    
    func reloadData(){
        engine.getProfile(){
            if $0?.isSuccess ?? false {
                self.lblName.text = $0?.fullname
                self.lblEmail.text = $0?.email
                self.lblRole.text = $0?.role
            }else {
                toast("Gagal")
            }
        }
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor(red: 195.0/255.0, green: 74.0/255.0, blue: 54.0/255.0, alpha: 1).cgColor
        let colorBottom = UIColor(red: 218.0/255.0, green: 140.0/255.0, blue: 25.0/255.0, alpha: 1).cgColor
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = CGRect(x: 0.0, y: 0.0, width: self.view.width - 16, height: self.viewGradasi.frame.size.height)
                
        self.viewGradasi.layer.insertSublayer(gradientLayer, at:0)
        viewGradasi.makeRoundedRect(withCornerRadius: 7)
    }
    
    @IBAction func btnListUserTapped(_ sender: Any) {
        ListUserVc.synth.show(from: self)
    }
    
    @IBAction func btnResetPasswordTapped(_ sender: Any) {
        
    }
    
    @IBAction func btnLogoutTapped(_ sender: Any) {
        
    }
}
