//
//  PagerViewCustomImageCell.swift
//  StarterKit
//
//  Created by Ridwan Surya Putra on 04/02/22.
//

import FSPagerView
import UIKit

class PagerViewCustomImageCell: FSPagerViewCell {
    //MARK: - Properties
    private let imgView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    private let labelProduk: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.textAlignment = .left
        lbl.font = UIFont.systemFont(ofSize: 18, weight: .light)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        view.setShadow(x: 0, y: 0, r: 4, a: 0.5, c: .darkGray)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override class func description() -> String {
        "PagerViewCustomImageCell"
    }
    
    
    //MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Helper
    
    func setup(image: UIImage?){
        self.imgView.image = image
    }
    
    func setupLabel(label: String?){
        self.labelProduk.text = label
        self.labelProduk.textAlignment = .center
    }
    
    private func configureUI(){
        self.backgroundColor = .white
        
        self.containerView.backgroundColor = .white
        self.containerView.addSubview(imgView)
        self.containerView.addSubview(labelProduk)
        self.addSubview(containerView)
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            containerView.widthAnchor.constraint(equalTo: containerView.heightAnchor,multiplier: 0.8),
            containerView.centerXAnchor.constraint(equalTo: centerXAnchor),
            
            imgView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor,constant: 8),
//            imgView.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: 0),
            imgView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
    //        imgView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            imgView.widthAnchor.constraint(equalTo: containerView.widthAnchor, multiplier: 0.9),
            imgView.heightAnchor.constraint(equalTo: imgView.widthAnchor),
            
            labelProduk.topAnchor.constraint(equalTo: containerView.topAnchor,constant: 30),
//            labelProduk.leftAnchor.constraint(equalTo: containerView.leftAnchor,constant: 18),
//            labelProduk.rightAnchor.constraint(equalTo: containerView.rightAnchor,constant: 18),
            labelProduk.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
    }
}
